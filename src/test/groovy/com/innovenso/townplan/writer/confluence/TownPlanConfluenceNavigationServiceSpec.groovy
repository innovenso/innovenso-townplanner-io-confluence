package com.innovenso.townplan.writer.confluence


import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.writer.confluence.dto.Page
import spock.lang.Ignore
import spock.lang.Specification

@Ignore
class TownPlanConfluenceNavigationServiceSpec extends Specification {
	def confluenceApiUrl = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_URL')
	def confluenceUser = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_USER')
	def confluenceApiKey = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_APIKEY')
	def confluenceSpaces = Arrays.asList(System.getenv('TOWNPLAN_WRITER_CONFLUENCE_SPACES').split(","))
	def confluenceMainMenu = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_MAINMENUNAME')
	def confluencePostfix = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_ARCHETYPEPAGEPOSTFIX')
	def confluenceBaseUrl = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_BASEURL')
	TownPlanImpl townPlan = new TownPlanImpl()
	SampleFactory samples = new SampleFactory(townPlan)

	ConfluenceGateway confluenceGateway = new ConfluenceGatewayFactory(confluenceApiUrl, confluenceUser, confluenceApiKey).confluenceGateway()

	ConfluenceSpaceConfig spaceConfig = new ConfluenceSpaceConfig(confluenceSpaces, confluenceMainMenu, confluencePostfix, confluenceBaseUrl)
	TownPlanConfluenceNavigationService serviceUnderTest = new TownPlanConfluenceNavigationService(confluenceGateway, spaceConfig)


	def "get existing page"() {
		given:
		String title = samples.title()
		serviceUnderTest.getOrCreateConfluenceNavigationPage(spaceConfig.spaces.first(), null, title)
		when:
		Optional<Page> foundPage = serviceUnderTest.getNavigationPage(spaceConfig.spaces.first(), title)
		then:
		foundPage.isPresent()
		println foundPage.get().id
	}

	def "create or get navigation page"() {
		when:
		Optional<String> createdPageId = serviceUnderTest.getOrCreateConfluenceNavigationPage(spaceConfig.spaces.first(), null, samples.title())
		then:
		createdPageId.isPresent()
		println createdPageId.get()
		cleanup:
		if (createdPageId.isPresent()) cleanupPage(createdPageId.get())
	}

	void cleanupPage(String pageId) {
		confluenceGateway.deletePage(pageId)
		confluenceGateway.purgePage(pageId)
	}
}
