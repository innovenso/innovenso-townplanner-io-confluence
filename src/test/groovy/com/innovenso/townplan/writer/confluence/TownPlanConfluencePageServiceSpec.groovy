package com.innovenso.townplan.writer.confluence

import com.innovenso.townplan.api.value.Concept
import com.innovenso.townplan.api.value.aspects.ContentOutputType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType
import com.innovenso.townplan.writer.confluence.dto.Page
import com.innovenso.townplan.writer.confluence.util.ConfluenceUtils
import com.thedeanda.lorem.LoremIpsum
import spock.lang.Ignore
import spock.lang.Specification

import java.nio.file.Files

@Ignore
class TownPlanConfluencePageServiceSpec extends Specification {
	def confluenceApiUrl = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_URL')
	def confluenceUser = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_USER')
	def confluenceApiKey = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_APIKEY')
	def confluenceSpaces = Arrays.asList(System.getenv('TOWNPLAN_WRITER_CONFLUENCE_SPACES').split(","))
	def confluenceMainMenu = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_MAINMENUNAME')
	def confluencePostfix = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_ARCHETYPEPAGEPOSTFIX')
	def confluenceBaseUrl = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_BASEURL')
	def writePath = com.google.common.io.Files.createTempDir().getAbsolutePath()
	TownPlanImpl townPlan = new TownPlanImpl()
	SampleFactory samples = new SampleFactory(townPlan)

	ConfluenceGateway confluenceGateway = new ConfluenceGatewayFactory(confluenceApiUrl, confluenceUser, confluenceApiKey).confluenceGateway()

	ConfluenceSpaceConfig spaceConfig = new ConfluenceSpaceConfig(confluenceSpaces, confluenceMainMenu, confluencePostfix, confluenceBaseUrl)
	TownPlanConfluenceNavigationService navigationService = new TownPlanConfluenceNavigationService(confluenceGateway, spaceConfig)
	TownPlanConfluenceAttachmentService attachmentService = new TownPlanConfluenceAttachmentService(writePath, "https://test.com", confluenceGateway)
	TownPlanConfluencePageService serviceUnderTest = new TownPlanConfluencePageService(confluenceGateway, spaceConfig, attachmentService, navigationService, "sparx.com")

	def "generate a Confluence page title from a concept"() {
		given:
		def concept = samples.enterprise()
		when:
		String pageTitle = serviceUnderTest.getConfluencePageTitle(concept)
		then:
		pageTitle == ConfluenceUtils.getConfluencePageTitle(concept)
	}

	def "generate the Confluence URL of a concept page"() {
		given:
		def concept = samples.enterprise()
		File markupFile = Files.createTempFile("spock", "page").toFile()
		String body = "<h1>Innovenso</h1><p>The Master Software Craftsman</p>"
		markupFile << body
		Page page = serviceUnderTest.createConfluencePageDtoForConcept("TEST", null, concept, markupFile).get()
		page.setId("123")
		when:
		String confluenceUrl = serviceUnderTest.getConfluencePageUrl(page)
		then:
		confluenceUrl == spaceConfig.confluenceBaseUrl + "/spaces/TEST/pages/123"
	}

	def "create a new Confluence page DTO for a concept"() {
		given:
		def concept = samples.enterprise()
		File markupFile = Files.createTempFile("spock", "page").toFile()
		String body = "<h1>Innovenso</h1><p>The Master Software Craftsman</p>"
		markupFile << body
		when:
		Optional<Page> page = serviceUnderTest.createConfluencePageDtoForConcept("TEST", null, concept, markupFile)
		then:
		page.isPresent()
		page.get().with {
			it.space.key == "TEST"
			it.title == serviceUnderTest.getConfluencePageTitle(concept)
			it.body.storage.value == body
		}
	}

	def "record the Confluence URL of a concept page"() {
		given:
		def concept = samples.enterprise()
		File markupFile = Files.createTempFile("spock", "page").toFile()
		String body = "<h1>Innovenso</h1><p>The Master Software Craftsman</p>"
		markupFile << body
		Page page = serviceUnderTest.createConfluencePageDtoForConcept("TEST", null, concept, markupFile).get()
		page.setId("123")

		when:
		serviceUnderTest.recordConfluenceUrl(townPlan, page, concept)
		then:
		concept.getContentDistributions(ContentOutputType.CONFLUENCE_URL).with {
			it.size() == 1
			it.first().getUrl() == spaceConfig.confluenceBaseUrl + "/spaces/TEST/pages/123"
		}
	}

	def "check if page exists"() {
		given:
		Page page = confluenceGateway.createPage(new Page(spaceConfig.spaces.first(), null, samples.title(), samples.description()))
		expect:
		serviceUnderTest.pageExists(page.id)
		!serviceUnderTest.pageExists("123")
		cleanup:
		cleanupPage(page.id)
	}

	def "get the existing page id of a concept based on the title"() {
		given:
		def concept = samples.enterprise()
		File markupFile = Files.createTempFile("spock", "page").toFile()
		String body = "<h1>Innovenso</h1><p>The Master Software Craftsman</p>"
		markupFile << body
		serviceUnderTest.createNewConfluencePageForConcept(townPlan, concept, "TEST", markupFile, ConfluenceNavigationPageType.ENTERPRISE)
		when:
		Optional<String> existingPageId = serviceUnderTest.getExistingPageIdForConceptBasedOnTitle("TEST", concept)
		Optional<String> nonExistingPageId = serviceUnderTest.getExistingPageIdForConceptBasedOnTitle("OTHER", concept)
		then:
		existingPageId.isPresent()
		println(existingPageId.get())
		nonExistingPageId.isEmpty()
	}

	def "get the existing page id of a concept"() {
		given:
		def concept = samples.enterprise()
		File markupFile = Files.createTempFile("spock", "page").toFile()
		String body = "<h1>Innovenso</h1><p>The Master Software Craftsman</p>"
		markupFile << body
		serviceUnderTest.createNewConfluencePageForConcept(townPlan, concept, "TEST", markupFile, ConfluenceNavigationPageType.ENTERPRISE)
		when:
		Optional<String> existingPageId = serviceUnderTest.getExistingPageIdBasedOnContentDistribution("TEST", concept)
		Optional<String> nonExistingPageId = serviceUnderTest.getExistingPageIdBasedOnContentDistribution("OTHER", concept)
		then:
		existingPageId.isPresent()
		println existingPageId
		nonExistingPageId.isEmpty()
	}

	def "check if a non-existing page exists in a space for a concept"() {
		given:
		def concept = samples.enterprise()
		expect:
		!serviceUnderTest.pageExistsForConcept("TEST", concept)
	}

	def "check if an actually existing page exists in a space for a concept"() {
		given:
		def concept = samples.enterprise()
		File markupFile = Files.createTempFile("spock", "page").toFile()
		String body = samples.description()
		markupFile << body
		Page page = serviceUnderTest.createConfluencePageDtoForConcept(spaceConfig.spaces.first(), null, concept, markupFile).get()
		Page resultingPage = confluenceGateway.createPage(page)
		serviceUnderTest.recordConfluenceUrl(townPlan, resultingPage, concept)
		expect:
		serviceUnderTest.pageExistsForConcept(spaceConfig.spaces.first(), concept)
		!serviceUnderTest.pageExistsForConcept("OTHER", concept)
		cleanup:
		cleanupPage(resultingPage.id)
	}

	def "create a new page for a concept in a space"() {
		given:
		Concept concept = samples.enterprise()
		File markupFile = sampleMarkupFile()
		when:
		Optional<Page> resultingPage = serviceUnderTest.createNewConfluencePageForConcept(townPlan, concept, "TEST", markupFile, ConfluenceNavigationPageType.ENTERPRISE)
		Optional<Page> pageWhichIsNotBeingCreatedBecauseItAlreadyExists = serviceUnderTest.createNewConfluencePageForConcept(townPlan, concept, "TEST", markupFile, ConfluenceNavigationPageType.ENTERPRISE)
		then:
		resultingPage.isPresent()
		resultingPage.get().id
		println resultingPage.get().id
		pageWhichIsNotBeingCreatedBecauseItAlreadyExists.isEmpty()
		serviceUnderTest.pageExistsForConcept("TEST", concept)
		cleanup:
		cleanupPage(resultingPage)
	}

	def "update an existing page for a concept in a space"() {
		given:
		Concept concept = samples.enterprise()
		File initialMarkupFile = sampleMarkupFile()
		String updatedMarkupBody = getSampleMarkup()
		File updatedMarkupFile = sampleMarkupFile(updatedMarkupBody)
		Optional<Page> createdPage = serviceUnderTest.createNewConfluencePageForConcept(townPlan, concept, "TEST", initialMarkupFile, ConfluenceNavigationPageType.ENTERPRISE)
		println createdPage.isPresent()
		Optional<String> createdPageId = serviceUnderTest.getExistingPageId("TEST", concept)
		println createdPageId
		println serviceUnderTest.pageExists(createdPageId.get())
		println serviceUnderTest.pageExistsForConcept("TEST", concept)
		when:
		Optional<Page> updatedPage = serviceUnderTest.updateConfluencePageForConcept(townPlan, concept, "TEST", updatedMarkupFile, ConfluenceNavigationPageType.ENTERPRISE)
		then:
		updatedPage.isPresent()
		updatedPage.get().with {
			it.body.storage.value == updatedMarkupBody
		}
		cleanup:
		cleanupPage(updatedPage)
	}


	static File sampleMarkupFile() {
		final File markupFile = Files.createTempFile("spock", "page").toFile()
		final String body = getSampleMarkup()
		markupFile << body
		return markupFile
	}

	static File sampleMarkupFile(String body) {
		final File markupFile = Files.createTempFile("spock", "page").toFile()
		markupFile << body
		return markupFile
	}

	static String getSampleMarkup() {
		LoremIpsum lorem = LoremIpsum.getInstance()
		return lorem.getHtmlParagraphs(1, 10)
	}

	void cleanupPage(String pageId) {
		confluenceGateway.deletePage(pageId)
		confluenceGateway.purgePage(pageId)
	}

	void cleanupPage(Optional<Page> page) {
		if (page.isPresent()) {
			String id = page.get().id
			if (id != null) cleanupPage(id)
		}
	}
}
