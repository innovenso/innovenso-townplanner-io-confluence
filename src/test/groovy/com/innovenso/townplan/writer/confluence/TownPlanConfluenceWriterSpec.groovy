package com.innovenso.townplan.writer.confluence

import com.google.common.io.Files
import com.innovenso.townplan.api.value.aspects.LifecyleType
import com.innovenso.townplan.api.value.it.ItPlatform
import com.innovenso.townplan.api.value.it.decision.DecisionContextType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.FileSystemAssetRepository
import spock.lang.Ignore
import spock.lang.Specification

@Ignore
class TownPlanConfluenceWriterSpec extends Specification {
	def confluenceApiUrl = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_URL')
	def confluenceUser = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_USER')
	def confluenceApiKey = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_APIKEY')
	def confluenceSpaces = Arrays.asList(System.getenv('TOWNPLAN_WRITER_CONFLUENCE_SPACES').split(","))
	def confluenceMainMenu = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_MAINMENUNAME')
	def confluencePostfix = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_ARCHETYPEPAGEPOSTFIX')
	def confluenceBaseUrl = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_BASEURL')
	def writePath = Files.createTempDir().getAbsolutePath()
	def assetRepository = new FileSystemAssetRepository(writePath, "https://test.com")
	TownPlanImpl townPlan = new TownPlanImpl()
	SampleFactory samples = new SampleFactory(townPlan)
	Map<String,File> renderedConcepts = [:]

	ConfluenceGateway confluenceGateway = new ConfluenceGatewayFactory(confluenceApiUrl, confluenceUser, confluenceApiKey).confluenceGateway()

	ConfluenceSpaceConfig spaceConfig = new ConfluenceSpaceConfig(confluenceSpaces, confluenceMainMenu, confluencePostfix, confluenceBaseUrl)
	TownPlanConfluenceNavigationService navigationService = new TownPlanConfluenceNavigationService(confluenceGateway, spaceConfig)
	TownPlanConfluenceAttachmentService attachmentService = new TownPlanConfluenceAttachmentService(writePath, "https://test.com", confluenceGateway)
	TownPlanConfluencePageService pageService = new TownPlanConfluencePageService(confluenceGateway, spaceConfig, attachmentService, navigationService, "sparx.com")
	TownPlanConfluenceWriter serviceUnderTest = new TownPlanConfluenceWriter(assetRepository, writePath, pageService, navigationService)

	def "create Confluence pages for a concept"() {
		given:
		samples.enterprise()
		when:
		serviceUnderTest.write(townPlan)
		then:
		true
	}

	def "writing a platform page does not result in a Freemarker exception"() {
		given:
		ItPlatform platform = samples.platform("platform_1", "The Flexible Platform")
		4.times {
			def system = samples.system(samples.id(), samples.title(), platform.key)
			3.times {
				samples.container(system)
			}
		}
		def enterprise = samples.enterprise()
		def capability0 = samples.capability(enterprise)
		def capability1 = samples.capability(enterprise, capability0)
		def buildingBlock = samples.buildingBlock(enterprise, capability1)
		samples.realizes(platform, buildingBlock)
		when:
		serviceUnderTest.getPlatformWriter().write(townPlan, platform, renderedConcepts)
		then:
		!renderedConcepts.isEmpty()
		renderedConcepts.keySet().contains(platform.key)
		renderedConcepts.get(platform.key).canRead()
		println renderedConcepts.get(platform.key).text
	}

	def "writing a system page does not result in a Freemarker exception"() {
		given:
		def enterprise = samples.enterprise()
		def system = samples.system()
		def system2 = samples.system()
		samples.setLifecycle(system.key, LifecyleType.PLANNED)
		def container = samples.container(system)
		samples.flow(system, system2)
		samples.integration(system, system2)
		def capability = samples.capability(enterprise)
		def block = samples.buildingBlock(enterprise, capability)
		samples.realizes(system, block)
		when:
		serviceUnderTest.getSystemWriter().write(townPlan, system, renderedConcepts)
		serviceUnderTest.getContainerWriter().write(townPlan, container, renderedConcepts)
		then:
		!renderedConcepts.isEmpty()
		renderedConcepts.keySet().contains(system.key)
		renderedConcepts.get(system.key).canRead()
		println renderedConcepts.get(system.key).text
		renderedConcepts.keySet().contains(container.key)
		renderedConcepts.get(container.key).canRead()
		println renderedConcepts.get(container.key).text
	}

	def "writing a decision page does not result in a Freemarker exception"() {
		given:
		def enterprise = samples.enterprise()
		def decision = samples.decision(enterprise)
		5.times {
			samples.decisionContext(decision, samples.randomEnum(DecisionContextType.class))
		}
		10.times {
			samples.option(decision)
		}

		Map<String,File> renderedConcepts = [:]
		when:
		serviceUnderTest.getDecisionWriter().write(townPlan, decision, renderedConcepts)
		then:
		!renderedConcepts.isEmpty()
		renderedConcepts.keySet().contains(decision.key)
		renderedConcepts.get(decision.key).canRead()
		println renderedConcepts.get(decision.key).text
	}

	def "writing a principle does not result in a Freemarker exception"() {
		given:
		def enterprise = samples.enterprise()
		def principle = samples.principle(enterprise)
		4.times {
			samples.descriptionDocumentation(principle)
		}
		samples.sparx(principle.getKey())

		Map<String,File> renderedConcepts = [:]
		when:
		serviceUnderTest.getPrincipleWriter().write(townPlan, principle, renderedConcepts)
		then:
		!renderedConcepts.isEmpty()
		renderedConcepts.keySet().contains(principle.key)
		renderedConcepts.get(principle.key).canRead()
		println renderedConcepts.get(principle.key).text
	}
}
