package com.innovenso.townplan.writer.confluence


import com.innovenso.townplan.api.value.Concept
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType
import com.innovenso.townplan.writer.confluence.dto.Page
import spock.lang.Ignore
import spock.lang.Specification

import java.nio.file.Files

@Ignore
class TownPlanConfluenceAttachmentServiceSpec extends Specification {
	def confluenceApiUrl = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_URL')
	def confluenceUser = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_USER')
	def confluenceApiKey = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_APIKEY')
	def confluenceSpaces = Arrays.asList(System.getenv('TOWNPLAN_WRITER_CONFLUENCE_SPACES').split(","))
	def confluenceMainMenu = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_MAINMENUNAME')
	def confluencePostfix = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_ARCHETYPEPAGEPOSTFIX')
	def confluenceBaseUrl = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_BASEURL')
	def writePath = com.google.common.io.Files.createTempDir().getAbsolutePath()
	def townPlan = new TownPlanImpl()
	def samples = new SampleFactory(townPlan)

	ConfluenceGateway confluenceGateway = new ConfluenceGatewayFactory(confluenceApiUrl, confluenceUser, confluenceApiKey).confluenceGateway()

	ConfluenceSpaceConfig spaceConfig = new ConfluenceSpaceConfig(confluenceSpaces, confluenceMainMenu, confluencePostfix, confluenceBaseUrl)
	TownPlanConfluenceNavigationService navigationService = new TownPlanConfluenceNavigationService(confluenceGateway, spaceConfig)
	TownPlanConfluenceAttachmentService serviceUnderTest = new TownPlanConfluenceAttachmentService(writePath, "https://test.com", confluenceGateway)
	TownPlanConfluencePageService pageService = new TownPlanConfluencePageService(confluenceGateway, spaceConfig, serviceUnderTest, navigationService, "sparx.com")


	def "upload attachments for a concept"() {
		given:
		Concept concept = samples.enterprise()
		File markupFile = sampleMarkupFile()
		Optional<Page> resultingPage = pageService.createNewConfluencePageForConcept(townPlan, concept, spaceConfig.spaces.first(), markupFile, ConfluenceNavigationPageType.SYSTEM)
		when:
		serviceUnderTest.attachImagesToPage(resultingPage.get().id, concept)
		then:
		true
		cleanup:
		cleanupPage(resultingPage)
	}


	File sampleMarkupFile() {
		final File markupFile = Files.createTempFile("spock", "page").toFile()
		final String body = samples.description()
		markupFile << body
		return markupFile
	}

	void cleanupPage(String pageId) {
		confluenceGateway.deletePage(pageId)
		confluenceGateway.purgePage(pageId)
	}

	void cleanupPage(Optional<Page> page) {
		if (page.isPresent()) {
			String id = page.get().id
			if (id != null) cleanupPage(id)
		}
	}
}
