package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.aspects.ExternalId;
import com.innovenso.townplan.api.value.aspects.ExternalIdType;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import com.innovenso.townplan.writer.confluence.model.ConceptConfluenceWriterModel;
import com.innovenso.townplan.writer.pipeline.freemarker.FreemarkerRenderer;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Log4j2
public abstract class AbstractTownPlanConfluenceConceptWriter<T extends Concept> {
	private final FreemarkerRenderer freemarkerRenderer;
	private final File targetBaseDirectory;
	private final TownPlanConfluencePageService confluencePageService;
	private final ConfluenceNavigationPageType navigationPageType;

	public AbstractTownPlanConfluenceConceptWriter(@NonNull final File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull final ConfluenceNavigationPageType navigationPageType) {
		this.freemarkerRenderer = new FreemarkerRenderer("confluence");
		this.targetBaseDirectory = targetBaseDirectory;
		if (!targetBaseDirectory.exists())
			targetBaseDirectory.mkdirs();
		this.confluencePageService = confluencePageService;
		this.navigationPageType = navigationPageType;
	}

	private Optional<File> getConfluenceWikiFile(final File targetConfluenceWikiFile, final T element,
			final Map<String, File> renderedConceptMap, final TownPlan townPlan) {
		log.debug("get Confluence wiki file for element {}", element.getKey());
		final Optional<Map<String, Object>> inputModel = Optional.of(Map.of("element",
				(Object) getModel(element, townPlan), "sparxUrl", getExternalLink(element, ExternalIdType.SPARX)));
		return inputModel.map(model -> freemarkerRenderer.write(model, getConfluenceWikiTemplateName(element) + ".ftl"))
				.filter(Optional::isPresent).map(Optional::get);
	}

	public Optional<File> write(TownPlan townPlan, T element, final Map<String, File> renderedConceptMap) {
		return prepareWikiFile(townPlan, element, renderedConceptMap)
				.map(preparedWikiFile -> writeConfluencePage(townPlan, element, preparedWikiFile));
	}

	public Optional<File> prepareWikiFile(TownPlan townPlan, T element, final Map<String, File> renderedConceptMap) {
		try {
			final File finalConfluenceWikiOutputFile = File.createTempFile(element.getKey(),
					UUID.randomUUID().toString());
			final Optional<File> confluenceWikiFile = getConfluenceWikiFile(finalConfluenceWikiOutputFile, element,
					renderedConceptMap, townPlan);
			return confluenceWikiFile.map(render -> {
				final File resultingFile = new File(targetBaseDirectory, element.getKey() + ".wiki");
				try {
					FileUtils.copyFile(render, resultingFile);
					log.debug("Confluence Wiki output file saved {}", resultingFile.getAbsolutePath());
					renderedConceptMap.put(element.getKey(), resultingFile);
				} catch (IOException e) {
					return null;
				}
				return resultingFile;
			});
		} catch (IOException e) {
			log.error(e);
			return Optional.empty();
		}
	}

	private File writeConfluencePage(TownPlan townPlan, T concept, File preparedWikiFile) {
		confluencePageService.createOrUpdateConfluencePagesForConcept(townPlan, concept, preparedWikiFile,
				navigationPageType);
		return preparedWikiFile;
	}

	protected String getConfluenceWikiTemplateName(T concept) {
		return concept.getType().toLowerCase();
	}

	protected ConceptConfluenceWriterModel<T> getModel(T element, @NonNull final TownPlan townPlan) {
		return new ConceptConfluenceWriterModel<>(element, townPlan);
	}

	protected String getExternalLink(T element, ExternalIdType externalIdType) {
		log.debug("get external link for element {}", element);
		return element.getExternalIds().stream()
				.filter(externalId -> externalId.getExternalIdType().equals(externalIdType)).peek(log::info).findAny()
				.map(this::getUrl).orElse("");
	}

	private String getUrl(ExternalId externalId) {
		if (externalId.getExternalIdType() == ExternalIdType.SPARX) {
			final String url = confluencePageService.getSparxBaseUrl() + externalId.getValue();
			log.debug(url);
			return url;
		}
		return null;
	}
}
