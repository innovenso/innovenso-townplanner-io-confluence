package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.it.principle.Principle;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluencePrincipleWriter extends AbstractTownPlanConfluenceConceptWriter<Principle> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluencePrincipleWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "principle"), confluencePageService,
				ConfluenceNavigationPageType.PRINCIPLE);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(Principle concept) {
		return "principle";
	}
}
