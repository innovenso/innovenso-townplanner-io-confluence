package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.principle.Principle;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.pipeline.AbstractTownPlanWriter;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.*;
import java.util.function.Consumer;

@Log4j2
public class TownPlanConfluenceWriter extends AbstractTownPlanWriter {
	private File targetBaseDirectory;
	@Getter
	private TownPlanConfluenceEnterpriseWriter enterpriseWriter;
	@Getter
	private TownPlanConfluenceBusinessCapabilityWriter businessCapabilityWriter;
	@Getter
	private TownPlanConfluenceBuildingBlockWriter buildingBlockWriter;
	@Getter
	private TownPlanConfluenceSystemWriter systemWriter;
	@Getter
	private TownPlanConfluenceContainerWriter containerWriter;
	@Getter
	private TownPlanConfluenceSystemIntegrationWriter integrationWriter;
	@Getter
	private TownPlanConfluenceProjectWriter projectWriter;
	@Getter
	private TownPlanConfluenceProjectMilestoneWriter milestoneWriter;
	@Getter
	private TownPlanConfluenceActorWriter actorWriter;
	@Getter
	private TownPlanConfluenceTeamWriter teamWriter;
	@Getter
	private TownPlanConfluenceDecisionWriter decisionWriter;
	@Getter
	private TownPlanConfluencePrincipleWriter principleWriter;
	@Getter
	private TownPlanConfluenceTechnologyWriter technologyWriter;
	@Getter
	private TownPlanConfluencePlatformWriter platformWriter;
	private final TownPlanConfluencePageService confluencePageService;
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceWriter(@NonNull AssetRepository assetRepository, @NonNull final String targetBasePath,
			@NonNull TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(assetRepository, ContentOutputType.CONFLUENCE_MARKUP);
		this.confluencePageService = confluencePageService;
		this.navigationService = navigationService;
		setTargetBaseDirectory(new File(targetBasePath, "confluence"));
	}

	@Override
	public void setTargetBaseDirectory(@NonNull final File targetBaseDirectory) {
		this.targetBaseDirectory = targetBaseDirectory;
		this.targetBaseDirectory.mkdirs();
		this.enterpriseWriter = new TownPlanConfluenceEnterpriseWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
		this.businessCapabilityWriter = new TownPlanConfluenceBusinessCapabilityWriter(this.targetBaseDirectory,
				confluencePageService, navigationService);
		this.buildingBlockWriter = new TownPlanConfluenceBuildingBlockWriter(this.targetBaseDirectory,
				confluencePageService, navigationService);
		this.systemWriter = new TownPlanConfluenceSystemWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
		this.containerWriter = new TownPlanConfluenceContainerWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
		this.integrationWriter = new TownPlanConfluenceSystemIntegrationWriter(this.targetBaseDirectory,
				confluencePageService, navigationService);
		this.projectWriter = new TownPlanConfluenceProjectWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
		this.milestoneWriter = new TownPlanConfluenceProjectMilestoneWriter(this.targetBaseDirectory,
				confluencePageService, navigationService);
		this.actorWriter = new TownPlanConfluenceActorWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
		this.teamWriter = new TownPlanConfluenceTeamWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
		this.decisionWriter = new TownPlanConfluenceDecisionWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
		this.principleWriter = new TownPlanConfluencePrincipleWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
		this.technologyWriter = new TownPlanConfluenceTechnologyWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
		this.platformWriter = new TownPlanConfluencePlatformWriter(this.targetBaseDirectory, confluencePageService,
				navigationService);
	}

	@Override
	public File getTargetBaseDirectory() {
		return targetBaseDirectory;
	}

	@Override
	protected void writeFiles(@NonNull TownPlan townPlan, @NonNull List<File> renderedFiles) {
		writeFiles(townPlan, renderedFiles, Optional.empty());
	}

	@Override
	protected void writeFiles(@NonNull TownPlan townPlan, @NonNull List<File> renderedFiles,
			@NonNull String selectedConcept) {
		writeFiles(townPlan, renderedFiles, Optional.of(selectedConcept));
	}

	private void writeFiles(@NonNull TownPlan townPlan, @NonNull List<File> renderedFiles,
			@NonNull Optional<String> selectedConcept) {
		final Map<String, File> renderedConcepts = new HashMap<>();
		withElements(townPlan, Enterprise.class, selectedConcept, element -> {
			enterpriseWriter.write(townPlan, element, renderedConcepts);
		});
		withElements(townPlan, BusinessCapability.class, selectedConcept,
				element -> businessCapabilityWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, ArchitectureBuildingBlock.class, selectedConcept,
				element -> buildingBlockWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, ItPlatform.class, selectedConcept,
				element -> platformWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, ItSystem.class, selectedConcept,
				element -> systemWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, ItContainer.class, selectedConcept,
				element -> containerWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, ItSystemIntegration.class, selectedConcept,
				element -> integrationWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, ItProject.class, selectedConcept,
				element -> projectWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, ItProjectMilestone.class, selectedConcept,
				element -> milestoneWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, BusinessActor.class, selectedConcept, element -> {
			if (element.isPerson())
				actorWriter.write(townPlan, element, renderedConcepts);
		});
		withElements(townPlan, BusinessActor.class, selectedConcept, element -> {
			if (element.isSquad())
				teamWriter.write(townPlan, element, renderedConcepts);
		});
		withElements(townPlan, Decision.class, selectedConcept,
				element -> decisionWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, Principle.class, selectedConcept,
				element -> principleWriter.write(townPlan, element, renderedConcepts));
		withElements(townPlan, Technology.class, selectedConcept,
				element -> technologyWriter.write(townPlan, element, renderedConcepts));
		renderedFiles.addAll(renderedConcepts.values());
	}

	private <T extends Element> void withElements(@NonNull TownPlan townPlan, Class<T> elementClass,
			Optional<String> selectedConceptKey, Consumer<T> consumer) {
		townPlan.getElements(elementClass).stream().sorted(Comparator.comparing(Concept::getTitle))
				.filter(element -> selectedConceptKey.map(key -> element.getKey().equals(key)).orElse(true))
				.forEach(consumer);
	}
}
