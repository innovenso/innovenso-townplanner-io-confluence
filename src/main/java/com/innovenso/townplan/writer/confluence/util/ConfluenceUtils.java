package com.innovenso.townplan.writer.confluence.util;

import com.innovenso.townplan.api.value.Concept;
import lombok.NonNull;

public class ConfluenceUtils {
	public static String getConfluencePageTitle(@NonNull final Concept concept) {
		return concept.getTitle() + " - " + concept.getType() + " (" + concept.getKey() + ")";
	}
}
