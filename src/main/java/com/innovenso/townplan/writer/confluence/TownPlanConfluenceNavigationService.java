package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import com.innovenso.townplan.writer.confluence.dto.Page;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.Objects;
import java.util.Optional;

@Log4j2
public class TownPlanConfluenceNavigationService {
	private final ConfluenceGateway confluenceGateway;
	private final ConfluenceSpaceConfig confluenceSpaceConfig;

	public TownPlanConfluenceNavigationService(@NonNull final ConfluenceGateway confluenceGateway,
			@NonNull final ConfluenceSpaceConfig confluenceSpaceConfig) {
		this.confluenceGateway = confluenceGateway;
		this.confluenceSpaceConfig = confluenceSpaceConfig;
	}

	public Optional<Page> getNavigationPage(String spaceKey, String title) {
		return confluenceGateway.getPages(spaceKey, title).getResults().stream().peek(log::info)
				.filter(page -> Objects.equals(page.getTitle(), title)).findFirst();
	}

	public boolean exists(String spaceKey, String title) {
		return getNavigationPage(spaceKey, title).isPresent();
	}

	public Optional<Page> createConfluencePageDtoForNavigation(String spaceKey, String parentPageId, String title) {
		return Optional.of(new Page(spaceKey, parentPageId, title, childPagesMacro()));
	}

	public Optional<String> getOrCreateConfluenceNavigationPage(String spaceKey, String parentId, String title) {
		if (exists(spaceKey, title)) {
			log.debug("page already exists {}", title);
			return getNavigationPage(spaceKey, title).map(Page::getId);
		} else {
			log.debug("creating new navigation page {}", title);
			return createConfluencePageDtoForNavigation(spaceKey, parentId, title).map(confluenceGateway::createPage)
					.map(Page::getId);
		}
	}

	public Optional<String> getRootPage(String spaceKey) {
		return getOrCreateConfluenceNavigationPage(spaceKey, null, confluenceSpaceConfig.mainMenuName());
	}

	public Optional<String> getCategoryPage(String spaceKey, ConfluenceNavigationPageType pageType) {
		return getRootPage(spaceKey)
				.flatMap(rootId -> getOrCreateConfluenceNavigationPage(spaceKey, rootId, pageType.category));
	}

	public Optional<String> getNavigationPage(String spaceKey, ConfluenceNavigationPageType pageType) {
		return getCategoryPage(spaceKey, pageType)
				.flatMap(rootId -> getOrCreateConfluenceNavigationPage(spaceKey, rootId, pageType.getLabel()));
	}

	public String childPagesMacro() {
		return "<ac:structured-macro ac:name=\"children\" ac:schema-version=\"2\" data-layout=\"default\" />";
	}
}
