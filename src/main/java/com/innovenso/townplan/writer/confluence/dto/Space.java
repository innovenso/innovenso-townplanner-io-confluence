package com.innovenso.townplan.writer.confluence.dto;

public class Space {
	String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
