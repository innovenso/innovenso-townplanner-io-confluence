package com.innovenso.townplan.writer.confluence.model;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionContext;
import com.innovenso.townplan.api.value.it.decision.DecisionContextType;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import lombok.NonNull;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DecisionConfluenceWriterModel extends ConceptConfluenceWriterModel<Decision> {
	public DecisionConfluenceWriterModel(@NonNull Decision concept, @NonNull final TownPlan townPlan) {
		super(concept, townPlan);
	}

	public List<BusinessActor> getApprovers() {
		return getPeople(RelationshipType.ACCOUNTABLE);
	}

	public List<BusinessActor> getOwners() {
		return getPeople(RelationshipType.RESPONSIBLE);
	}

	public List<BusinessActor> getContributors() {
		return getPeople(RelationshipType.HAS_BEEN_CONSULTED);
	}

	public List<BusinessActor> getInformed() {
		return getPeople(RelationshipType.HAS_BEEN_INFORMED);
	}

	public List<BusinessActor> getPeople(@NonNull final RelationshipType relationshipType) {
		return getConcept().getIncomingRelationships(BusinessActor.class).stream()
				.filter(relationship -> relationship.getRelationshipType().equals(relationshipType))
				.map(Relationship::getSource).map(BusinessActor.class::cast)
				.sorted(Comparator.comparing(BusinessActor::getTitle)).collect(Collectors.toList());
	}

	public List<DecisionContext> getCurrentStates() {
		return getContexts(DecisionContextType.CURRENT_STATE);
	}

	public List<DecisionContext> getGoals() {
		return getContexts(DecisionContextType.GOAL);
	}

	public List<DecisionContext> getConsequences() {
		return getContexts(DecisionContextType.CONSEQUENCE);
	}

	public List<DecisionContext> getAssumptions() {
		return getContexts(DecisionContextType.ASSUMPTION);
	}

	public List<DecisionOption> getOptions() {
		return getConcept().getOptions().stream()
				.sorted(Comparator.comparing(Concept::getTotalScoreAsPercentage).reversed())
				.collect(Collectors.toList());
	}

	public List<DecisionContext> getContexts(DecisionContextType contextType) {
		return getConcept().getContexts().stream().filter(context -> context.getContextType().equals(contextType))
				.sorted(Comparator.comparing(DecisionContext::getSortKey)).collect(Collectors.toList());
	}

	public String getColor(@NonNull DecisionOption option) {
		switch (option.getVerdict()) {
			case CHOSEN :
				return "#e3fcef";
			case REJECTED :
				return "#ffebe6";
			default :
				return "#ffffff";
		}
	}
}
