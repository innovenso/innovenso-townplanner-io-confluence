package com.innovenso.townplan.writer.confluence.model;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.enums.TechnologyRecommendation;
import com.innovenso.townplan.api.value.enums.TechnologyType;
import com.innovenso.townplan.api.value.it.HasTechnologies;
import com.innovenso.townplan.api.value.it.Technology;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class TechnologyRadarConfluenceWriterModel<T extends Concept> {
	private final T concept;
	private final TownPlan townPlan;

	public TechnologyRadarConfluenceWriterModel(@NonNull T concept, @NonNull TownPlan townPlan) {
		this.concept = concept;
		this.townPlan = townPlan;
	}

	public List<Technology> getTechnologies(@NonNull final String technologyType,
			@NonNull final String recommendation) {
		return TechnologyRecommendation.getTechnologyRecommendation(recommendation)
				.map(reco -> getTechnologies(
						TechnologyType.getTechnologyType(technologyType).orElse(TechnologyType.UNDEFINED)).stream()
								.peek(log::info).filter(technology -> reco.equals(technology.getRecommendation()))
								.collect(Collectors.toList()))
				.orElse(List.of());
	}

	public List<TechnologyType> getQuadrants() {
		return TechnologyType.getQuadrants();
	}

	public List<TechnologyRecommendation> getQuadrantSections() {
		return TechnologyRecommendation.getRadarSections();
	}

	public String getQuadrantTitle(@NonNull final String technologyType) {
		return TechnologyType.getTechnologyType(technologyType).map(TechnologyType::getLabel).orElse("Undefined");
	}

	public String getRecommendationtitle(@NonNull final String recommendation) {
		return TechnologyRecommendation.getTechnologyRecommendation(recommendation)
				.map(TechnologyRecommendation::getLabel).orElse("");
	}

	private List<Technology> getTechnologies(@NonNull final TechnologyType technologyType) {
		return technologies().filter(technology -> Objects.equals(technologyType, technology.getTechnologyType()))
				.sorted(Comparator.comparing(Technology::getTitle)).collect(Collectors.toList());
	}

	private Stream<Technology> technologies() {
		if (concept instanceof HasTechnologies)
			return ((HasTechnologies) concept).getTechnologies().stream();
		else if (concept instanceof Enterprise)
			return townPlan.getElements(Technology.class).stream();
		else
			return Stream.empty();
	}

	public boolean hasTechnologies() {
		return !technologies().collect(Collectors.toSet()).isEmpty();
	}
}
