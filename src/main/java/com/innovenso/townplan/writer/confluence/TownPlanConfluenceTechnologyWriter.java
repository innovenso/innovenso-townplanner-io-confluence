package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.it.Technology;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceTechnologyWriter extends AbstractTownPlanConfluenceConceptWriter<Technology> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceTechnologyWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "technology"), confluencePageService,
				ConfluenceNavigationPageType.TECHNOLOGY);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(Technology concept) {
		return "technology";
	}
}
