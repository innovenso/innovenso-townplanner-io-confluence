package com.innovenso.townplan.writer.confluence;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import feign.Feign;
import feign.Logger;
import feign.auth.BasicAuthRequestInterceptor;
import feign.form.FormEncoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.NonNull;

public record ConfluenceGatewayFactory(@NonNull String url, @NonNull String user, @NonNull String apikey) {

	public ConfluenceGateway confluenceGateway() {
		return Feign.builder().requestInterceptor(new BasicAuthRequestInterceptor(user, apikey))
				.logLevel(Logger.Level.FULL).encoder(new FormEncoder(new JacksonEncoder(feignObjectMapper())))
				.decoder(new JacksonDecoder(feignObjectMapper())).decode404().target(ConfluenceGateway.class, url);
	}

	public ObjectMapper feignObjectMapper() {
		ObjectMapper om = new ObjectMapper();
		om.registerModule(new JavaTimeModule());

		om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		om.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
		om.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
		om.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL);
		om.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		om.disable(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE);
		return om;
	}
}
