package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.TownPlanIO;
import com.innovenso.townplan.reader.TownPlanReader;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.TownPlanWriter;
import lombok.NonNull;
import lombok.Value;

import java.util.Optional;

@Value
public class TownPlanConfluenceIO implements TownPlanIO {
	ConfluenceGateway confluenceGateway;
	TownPlanConfluenceNavigationService navigationService;
	TownPlanConfluenceAttachmentService attachmentService;
	TownPlanConfluencePageService pageService;
	AssetRepository assetRepository;
	String targetBasePath;

	public TownPlanConfluenceIO(@NonNull AssetRepository assetRepository,
			@NonNull ConfluenceGatewayConfig confluenceGatewayConfig,
			@NonNull ConfluenceSpaceConfig confluenceSpaceConfig,
			@NonNull ConfluenceAttachmentConfig confluenceAttachmentConfig, @NonNull String sparxBaseUrl,
			@NonNull String targetBasePath) {
		this.confluenceGateway = new ConfluenceGatewayFactory(confluenceGatewayConfig.url(),
				confluenceGatewayConfig.user(), confluenceGatewayConfig.apiKey()).confluenceGateway();
		this.navigationService = new TownPlanConfluenceNavigationService(confluenceGateway, confluenceSpaceConfig);
		this.attachmentService = new TownPlanConfluenceAttachmentService(confluenceAttachmentConfig.assetLocalPath(),
				confluenceAttachmentConfig.contentDistributionUrl(), confluenceGateway);
		this.pageService = new TownPlanConfluencePageService(confluenceGateway, confluenceSpaceConfig,
				attachmentService, navigationService, sparxBaseUrl);
		this.assetRepository = assetRepository;
		this.targetBasePath = targetBasePath;
	}

	@Override
	public Optional<TownPlanReader> getReader() {
		return Optional.empty();
	}

	@Override
	public Optional<TownPlanWriter> getWriter() {
		return Optional
				.of(new TownPlanConfluenceWriter(assetRepository, targetBasePath, pageService, navigationService));
	}

	@Override
	public String getKey() {
		return "confluence";
	}

	@Override
	public String getTitle() {
		return "Confluence";
	}

	@Override
	public String getDescription() {
		return "Write the townplan to Confluence";
	}
}
