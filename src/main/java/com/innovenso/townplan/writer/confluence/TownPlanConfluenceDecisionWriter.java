package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import com.innovenso.townplan.writer.confluence.model.ConceptConfluenceWriterModel;
import com.innovenso.townplan.writer.confluence.model.DecisionConfluenceWriterModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceDecisionWriter extends AbstractTownPlanConfluenceConceptWriter<Decision> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceDecisionWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "decision"), confluencePageService, ConfluenceNavigationPageType.DECISION);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(Decision concept) {
		return "decision";
	}

	@Override
	protected ConceptConfluenceWriterModel<Decision> getModel(@NonNull final Decision element,
			@NonNull final TownPlan townPlan) {
		return new DecisionConfluenceWriterModel(element, townPlan);
	}
}
