package com.innovenso.townplan.writer.confluence;

import lombok.NonNull;

public record ConfluenceAttachmentConfig(@NonNull String assetLocalPath, @NonNull String contentDistributionUrl) {
}
