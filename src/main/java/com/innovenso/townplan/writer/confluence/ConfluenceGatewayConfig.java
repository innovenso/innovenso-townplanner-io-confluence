package com.innovenso.townplan.writer.confluence;

import lombok.NonNull;

public record ConfluenceGatewayConfig(@NonNull String url, @NonNull String user, @NonNull String apiKey) {
}
