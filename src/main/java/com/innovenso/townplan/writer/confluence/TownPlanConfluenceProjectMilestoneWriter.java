package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import com.innovenso.townplan.writer.confluence.model.ConceptConfluenceWriterModel;
import com.innovenso.townplan.writer.confluence.model.MilestoneConfluenceWriterModel;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceProjectMilestoneWriter
		extends
			AbstractTownPlanConfluenceConceptWriter<ItProjectMilestone> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceProjectMilestoneWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "milestone"), confluencePageService,
				ConfluenceNavigationPageType.MILESTONE);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(ItProjectMilestone concept) {
		return "milestone";
	}

	@Override
	protected ConceptConfluenceWriterModel<ItProjectMilestone> getModel(ItProjectMilestone element,
			@NonNull final TownPlan townPlan) {
		return new MilestoneConfluenceWriterModel(element, townPlan);
	}
}
