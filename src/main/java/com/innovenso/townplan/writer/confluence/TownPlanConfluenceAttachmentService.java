package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.writer.confluence.dto.request.CreateUpdateAttachmentRequest;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
public class TownPlanConfluenceAttachmentService {
	private final File assetLocalDirectory;
	private final String contentDistributionBaseUrl;
	private final ConfluenceGateway confluenceGateway;

	public TownPlanConfluenceAttachmentService(@NonNull String assetLocalPath,
			@NonNull String contentDistributionBaseUrl, @NonNull ConfluenceGateway confluenceGateway) {
		this.assetLocalDirectory = new File(assetLocalPath);
		this.contentDistributionBaseUrl = contentDistributionBaseUrl;
		this.confluenceGateway = confluenceGateway;
	}

	public Set<File> getSvgImages(@NonNull final Concept concept) {
		return getFiles(concept, ContentOutputType.SVG);
	}

	public Set<File> getAttachments(@NonNull final Concept concept) {
		return concept.getAttachments().stream()
				.filter(attachment -> attachment.getCdnContentUrl().startsWith(contentDistributionBaseUrl))
				.map(attachment -> StringUtils.removeStart(attachment.getCdnContentUrl(), contentDistributionBaseUrl))
				.map(path -> Path.of(assetLocalDirectory.getAbsolutePath(), path).toFile()).filter(File::canRead)
				.collect(Collectors.toSet());
	}

	public Set<File> getFiles(@NonNull final Concept concept, @NonNull final ContentOutputType contentOutputType) {
		return concept.getContentDistributions(contentOutputType).stream()
				.filter(contentDistribution -> contentDistribution.getUrl().startsWith(contentDistributionBaseUrl))
				.map(contentDistribution -> StringUtils.removeStart(contentDistribution.getUrl(),
						contentDistributionBaseUrl))
				.map(path -> Path.of(assetLocalDirectory.getAbsolutePath(), path).toFile()).filter(File::canRead)
				.collect(Collectors.toSet());
	}

	public Set<File> getExcelFiles(@NonNull final Concept concept) {
		return getFiles(concept, ContentOutputType.EXCEL);
	}

	public void attachImagesToPage(@NonNull final String pageId, @NonNull final Concept concept) {
		Set.of(ContentOutputType.SVG, ContentOutputType.ARCHIMATE, ContentOutputType.EXCEL, ContentOutputType.ZIP,
				ContentOutputType.LATEX)
				.forEach(contentOutputType -> getFiles(concept, contentOutputType)
						.forEach(file -> updateAttachmentToPage(pageId, file, file.getName())));
	}

	public void attachAttachmentsToPage(@NonNull final String pageId, @NonNull final Concept concept) {
		getAttachments(concept).forEach(file -> updateAttachmentToPage(pageId, file, file.getName()));
	}

	private void updateAttachmentToPage(String pageId, File file, String filename) {
		try {
			confluenceGateway.updateAttachmentToPage(pageId, new CreateUpdateAttachmentRequest(file, file.getName()));
		} catch (Exception e) {
			log.error("Cannot update attachment on Confluence page", e);
		}
	}
}
