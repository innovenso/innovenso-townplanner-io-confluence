package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.it.ItProject;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceProjectWriter extends AbstractTownPlanConfluenceConceptWriter<ItProject> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceProjectWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "project"), confluencePageService, ConfluenceNavigationPageType.PROJECT);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(ItProject concept) {
		return "project";
	}
}
