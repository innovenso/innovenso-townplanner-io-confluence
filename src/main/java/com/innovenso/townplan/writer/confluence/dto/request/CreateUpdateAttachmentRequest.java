package com.innovenso.townplan.writer.confluence.dto.request;

import java.io.File;

public class CreateUpdateAttachmentRequest {
	File file;
	String minorEdit;
	String comment;

	public CreateUpdateAttachmentRequest(File file, String comment) {
		this.file = file;
		this.minorEdit = "true";
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getMinorEdit() {
		return minorEdit;
	}

	public void setMinorEdit(String minorEdit) {
		this.minorEdit = minorEdit;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
}
