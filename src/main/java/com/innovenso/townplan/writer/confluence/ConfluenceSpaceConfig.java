package com.innovenso.townplan.writer.confluence;

import lombok.NonNull;

import java.util.List;

public record ConfluenceSpaceConfig(@NonNull List<String> spaces, @NonNull String mainMenuName,
		@NonNull String archetypePagePostfix, @NonNull String confluenceBaseUrl) {
}
