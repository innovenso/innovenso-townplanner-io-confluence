package com.innovenso.townplan.writer.confluence.dto;

import java.util.List;
import java.util.Objects;

import static java.util.Collections.singletonList;

public class Page {
	String id;
	String title;
	PageVersion version;
	Type type;
	Space space;
	PageBody body;
	List<Page> ancestors;
	String status;

	public Page() {
	}

	public Page(String id, long version, String title, String body) {
		PageBody.Storage storage = new PageBody.Storage();
		storage.setValue(body);
		storage.setRepresentation(PageBody.Representation.STORAGE);
		PageBody pageBody = new PageBody();
		pageBody.setStorage(storage);

		PageVersion pageVersion = new PageVersion();
		pageVersion.setNumber(version);
		pageVersion.setMinorEdit(true);

		this.setId(id);
		this.setBody(pageBody);
		this.setVersion(pageVersion);
		this.setTitle(title);
		this.setType(Type.PAGE);
	}

	public Page(String spaceKey, String parentPageId, String title, String body) {
		PageBody.Storage storage = new PageBody.Storage();
		storage.setValue(body);
		storage.setRepresentation(PageBody.Representation.STORAGE);
		PageBody pageBody = new PageBody();
		pageBody.setStorage(storage);

		Space space = new Space();
		space.setKey(spaceKey);

		if (parentPageId != null) {
			Page parentPage = new Page();
			parentPage.setId(parentPageId);
			this.setAncestors(singletonList(parentPage));
		}

		PageVersion pageVersion = new PageVersion();
		pageVersion.setMinorEdit(true);

		this.setTitle(title);
		this.setSpace(space);
		this.setType(Type.PAGE);
		this.setVersion(pageVersion);
		this.setBody(pageBody);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Page> getAncestors() {
		return ancestors;
	}

	public void setAncestors(List<Page> ancestors) {
		this.ancestors = ancestors;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PageBody getBody() {
		return body;
	}

	public void setBody(PageBody body) {
		this.body = body;
	}

	public Space getSpace() {
		return space;
	}

	public void setSpace(Space space) {
		this.space = space;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public PageVersion getVersion() {
		return version;
	}

	public void setVersion(PageVersion version) {
		this.version = version;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Page{" + "id='" + id + '\'' + ", title='" + title + '\'' + ", version=" + version + ", type=" + type
				+ ", space=" + space + ", body=" + body + ", ancestors=" + ancestors + ", status='" + status + '\''
				+ '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Page page = (Page) o;
		return Objects.equals(id, page.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public enum Type {
		PAGE("page");

		private final String key;

		Type(String key) {
			this.key = key;
		}

		public String getKey() {
			return key;
		}

		@Override
		public String toString() {
			return getKey();
		}
	}
}
