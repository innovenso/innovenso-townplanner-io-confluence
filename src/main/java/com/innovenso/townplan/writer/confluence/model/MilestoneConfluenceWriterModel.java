package com.innovenso.townplan.writer.confluence.model;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.NonNull;

import java.util.Set;
import java.util.stream.Collectors;

public class MilestoneConfluenceWriterModel extends ConceptConfluenceWriterModel<ItProjectMilestone> {

	public MilestoneConfluenceWriterModel(@NonNull ItProjectMilestone concept, @NonNull final TownPlan townPlan) {
		super(concept, townPlan);
	}

	public Set<Relationship> getSystemImpacts() {
		return getImpacts(ItSystem.class);
	}

	public Set<Relationship> getBuildingBlockImpacts() {
		return getImpacts(ArchitectureBuildingBlock.class);
	}

	public Set<Relationship> getCapabilityImpacts() {
		return getImpacts(BusinessCapability.class);
	}

	public Set<Relationship> getIntegrationImpacts() {
		return getImpacts(ItSystemIntegration.class);
	}

	private Set<Relationship> getImpacts(Class<? extends Element> elementClass) {
		return getConcept().getOutgoingRelationships().stream()
				.filter(relationship -> elementClass.isInstance(relationship.getTarget())).filter(this::hasImpact)
				.collect(Collectors.toSet());
	}

	private boolean hasImpact(@NonNull final Relationship relationship) {
		return Set.of(RelationshipType.IMPACT_CHANGE, RelationshipType.IMPACT_CREATE, RelationshipType.IMPACT_REMOVE,
				RelationshipType.IMPACT_REMAINS_UNCHANGED).contains(relationship.getRelationshipType());
	}
}
