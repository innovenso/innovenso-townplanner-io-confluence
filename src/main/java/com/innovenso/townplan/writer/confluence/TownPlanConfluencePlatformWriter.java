package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.it.ItPlatform;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluencePlatformWriter extends AbstractTownPlanConfluenceConceptWriter<ItPlatform> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluencePlatformWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "platform"), confluencePageService, ConfluenceNavigationPageType.PLATFORM);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(ItPlatform concept) {
		return "platform";
	}
}
