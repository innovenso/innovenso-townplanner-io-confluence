package com.innovenso.townplan.writer.confluence.dto;

public class PageVersion {
	Long number;
	boolean minorEdit = true;

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public boolean isMinorEdit() {
		return minorEdit;
	}

	public void setMinorEdit(boolean minorEdit) {
		this.minorEdit = minorEdit;
	}

	@Override
	public String toString() {
		return "PageVersion{" + "number=" + number + ", minorEdit=" + minorEdit + '}';
	}
}
