package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceSystemWriter extends AbstractTownPlanConfluenceConceptWriter<ItSystem> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceSystemWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "system"), confluencePageService, ConfluenceNavigationPageType.SYSTEM);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(ItSystem concept) {
		return "system";
	}
}
