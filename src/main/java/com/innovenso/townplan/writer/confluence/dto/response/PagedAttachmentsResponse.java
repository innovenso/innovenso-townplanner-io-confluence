package com.innovenso.townplan.writer.confluence.dto.response;

import com.innovenso.townplan.writer.confluence.dto.Attachment;

import java.util.List;

public class PagedAttachmentsResponse {
	List<Attachment> results;

	public List<Attachment> getResults() {
		return results;
	}

	public void setResults(List<Attachment> results) {
		this.results = results;
	}
}
