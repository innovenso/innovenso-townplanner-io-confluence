package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceBusinessCapabilityWriter
		extends
			AbstractTownPlanConfluenceConceptWriter<BusinessCapability> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceBusinessCapabilityWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "capability"), confluencePageService,
				ConfluenceNavigationPageType.CAPABILITY);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(BusinessCapability concept) {
		return "capability";
	}
}
