package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceEnterpriseWriter extends AbstractTownPlanConfluenceConceptWriter<Enterprise> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceEnterpriseWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "enterprise"), confluencePageService,
				ConfluenceNavigationPageType.ENTERPRISE);
		this.navigationService = navigationService;
	}
}
