package com.innovenso.townplan.writer.confluence.dto;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum ConfluenceNavigationPageType {
	ENTERPRISE("Enterprises", "Strategy"), CAPABILITY("Business Capabilities", "Strategy"), BUILDING_BLOCK(
			"Building Blocks",
			"Strategy"), SYSTEM("Systems", "Application"), PLATFORM("Platforms", "Application"), CONTAINER("Containers",
					"Application"), INTEGRATION("System Integrations", "Application"), TECHNOLOGY("Technologies",
							"Technology"), TECHRADAR("Technology Radar", "Technology"), ENTITY("Entities",
									"Application"), ACTOR("People", "Business"), TEAM("Teams", "Business"), PROJECT(
											"Projects", "Implementation"), MILESTONE("Project Milestones",
													"Implementation"), DECISION("Architecture Decisions",
															"Motivation"), PRINCIPLE("Principle",
																	"Motivation"), VIEW("Process Flows", "Views");

	public final String label;
	public final String category;

	public String getLabel() {
		return label;
	}

	public String getCategory() {
		return category;
	}

	ConfluenceNavigationPageType(String label, String category) {
		this.label = label;
		this.category = category;
	}

	public static Optional<ConfluenceNavigationPageType> getConfluenceNavigationPageType(final String value) {
		if (value == null)
			return Optional.empty();
		try {
			return Optional.of(ConfluenceNavigationPageType.valueOf(value.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}

	public String getName() {
		return name();
	}

	public static Set<String> names() {
		return Arrays.stream(values()).map(Enum::name).collect(Collectors.toSet());
	}
}
