package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceBuildingBlockWriter
		extends
			AbstractTownPlanConfluenceConceptWriter<ArchitectureBuildingBlock> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceBuildingBlockWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "block"), confluencePageService,
				ConfluenceNavigationPageType.BUILDING_BLOCK);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(ArchitectureBuildingBlock concept) {
		return "block";
	}
}
