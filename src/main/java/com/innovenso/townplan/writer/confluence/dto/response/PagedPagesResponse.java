package com.innovenso.townplan.writer.confluence.dto.response;

import com.innovenso.townplan.writer.confluence.dto.Page;

import java.util.List;

public class PagedPagesResponse {
	List<Page> results;

	public List<Page> getResults() {
		return results == null ? List.of() : results;
	}

	public void setResults(List<Page> results) {
		this.results = results;
	}
}
