package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.command.AddContentDistributionAspectCommand;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.aspects.ContentDistribution;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.domain.TownPlanImpl;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import com.innovenso.townplan.writer.confluence.dto.Page;
import com.innovenso.townplan.writer.confluence.dto.PageVersion;
import com.innovenso.townplan.writer.confluence.util.ConfluenceUtils;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Log4j2
public class TownPlanConfluencePageService {
	private final ConfluenceGateway confluenceGateway;
	private final ConfluenceSpaceConfig confluenceSpaceConfig;
	private final TownPlanConfluenceAttachmentService attachmentService;
	private final TownPlanConfluenceNavigationService navigationService;
	@Getter
	private final String sparxBaseUrl;

	public TownPlanConfluencePageService(@NonNull final ConfluenceGateway confluenceGateway,
			@NonNull final ConfluenceSpaceConfig confluenceSpaceConfig,
			@NonNull final TownPlanConfluenceAttachmentService attachmentService,
			@NonNull final TownPlanConfluenceNavigationService navigationService,
			@NonNull final @Value("${external.sparx.url.base}") String sparxBaseUrl) {
		this.confluenceGateway = confluenceGateway;
		this.confluenceSpaceConfig = confluenceSpaceConfig;
		this.attachmentService = attachmentService;
		this.navigationService = navigationService;
		this.sparxBaseUrl = sparxBaseUrl;
	}

	public Optional<String> getExistingPageId(String spaceId, Concept concept) {
		return getExistingPageIdForConceptBasedOnTitle(spaceId, concept)
				.or(() -> getExistingPageIdBasedOnContentDistribution(spaceId, concept));
	}

	public Optional<String> getExistingPageIdBasedOnContentDistribution(String spaceId, Concept concept) {
		try {
			return concept.getContentDistributions(ContentOutputType.CONFLUENCE_URL).stream()
					.filter(contentDistribution -> contentDistribution.getUrl()
							.startsWith(confluenceSpaceConfig.confluenceBaseUrl()))
					.filter(contentDistribution -> contentDistribution.getUrl().contains("spaces/" + spaceId + "/"))
					.findFirst().map(ContentDistribution::getUrl).map(url -> url.substring(url.lastIndexOf('/') + 1));
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	public Optional<String> getExistingPageIdForConceptBasedOnTitle(String spaceKey, Concept concept) {
		final String confluencePageTitle = getConfluencePageTitle(concept);
		return confluenceGateway.getPages(spaceKey, confluencePageTitle).getResults().stream()
				.peek(page -> log.debug("page {} exists in space {}", page.getTitle(), spaceKey))
				.filter(page -> Objects.equals(page.getTitle(), confluencePageTitle)).findFirst().map(Page::getId);
	}

	public String getConfluencePageTitle(Concept concept) {
		return ConfluenceUtils.getConfluencePageTitle(concept);
	}

	public String getConfluencePageUrl(Page page) {
		return confluenceSpaceConfig.confluenceBaseUrl() + "/spaces/" + page.getSpace().getKey() + "/pages/"
				+ page.getId();
	}

	public Optional<Page> createConfluencePageDtoForConcept(String spaceKey, String parentPageId, Concept concept,
			File markupFile) {
		try {
			Page page = new Page(spaceKey, parentPageId, getConfluencePageTitle(concept),
					FileUtils.readFileToString(markupFile, "UTF-8"));
			return Optional.of(page);
		} catch (IOException e) {
			return Optional.empty();
		}
	}

	public void createOrUpdateConfluencePagesForConcept(TownPlan townPlan, Concept concept, File markupFile,
			ConfluenceNavigationPageType confluenceNavigationPageType) {
		confluenceSpaceConfig.spaces().forEach(spaceKey -> {
			Optional<Page> resultingPage = pageExistsForConcept(spaceKey, concept)
					? updateConfluencePageForConcept(townPlan, concept, spaceKey, markupFile,
							confluenceNavigationPageType)
					: createNewConfluencePageForConcept(townPlan, concept, spaceKey, markupFile,
							confluenceNavigationPageType);
			resultingPage.ifPresent(page -> {
				if (page.getId() != null) {
					attachmentService.attachImagesToPage(page.getId(), concept);
					attachmentService.attachAttachmentsToPage(page.getId(), concept);
				}
			});
		});
	}

	public Optional<Page> createNewConfluencePageForConcept(TownPlan townPlan, Concept concept, String spaceKey,
			File markupFile, ConfluenceNavigationPageType navigationPageType) {
		if (pageExistsForConcept(spaceKey, concept)) {
			log.debug("page for concept {} already exists in space {}", concept, spaceKey);
			return Optional.empty();
		} else {
			log.debug("creating page for concept {} in space {}", concept, spaceKey);
			return createConfluencePageDtoForConcept(spaceKey,
					navigationService.getNavigationPage(spaceKey, navigationPageType).orElse(null), concept, markupFile)
							.map(this::createPage)
							.map(resultingPage -> recordConfluenceUrl(townPlan, resultingPage, concept));
		}
	}

	private Page createPage(Page page) {
		try {
			return confluenceGateway.createPage(page);
		} catch (Exception e) {
			log.error("Can't create page", e);
			return null;
		}
	}

	private Page updatePage(String id, Page page) {
		try {
			return confluenceGateway.updatePage(id, page);
		} catch (Exception e) {
			log.error("Can't update page", e);
			return null;
		}
	}

	public Optional<Page> updateConfluencePageForConcept(TownPlan townPlan, Concept concept, String spaceKey,
			File markupFile, ConfluenceNavigationPageType navigationPageType) {
		if (!pageExistsForConcept(spaceKey, concept)) {
			log.warn("page for concept {} does not exist, cannot update", concept.getKey());
			return Optional.empty();
		}
		Optional<String> existingPageId = getExistingPageId(spaceKey, concept);
		log.debug("updating page {}", existingPageId.orElse("unknown"));
		if (existingPageId.isEmpty())
			return Optional.empty();
		Page existingPage = confluenceGateway.getPage(existingPageId.get());
		return createConfluencePageDtoForConcept(spaceKey,
				navigationService.getNavigationPage(spaceKey, navigationPageType).orElse(null), concept, markupFile)
						.map(pageDto -> {
							PageVersion newVersion = new PageVersion();
							newVersion.setNumber(existingPage.getVersion().getNumber() + 1);
							newVersion.setMinorEdit(true);
							pageDto.setVersion(newVersion);
							return this.updatePage(existingPageId.get(), pageDto);
						});
	}

	public boolean pageExists(String id) {
		return confluenceGateway.getPage(id).getId() != null;
	}

	public boolean pageExistsForConcept(String spaceKey, Concept concept) {
		return getExistingPageId(spaceKey, concept).map(this::pageExists).orElse(false);
	}

	public Page recordConfluenceUrl(@NonNull final TownPlan townPlan, @NonNull Page page, @NonNull Concept concept) {
		Optional.of(townPlan).filter(TownPlanImpl.class::isInstance).map(TownPlanImpl.class::cast)
				.ifPresent(townPlanImpl -> townPlanImpl.execute(AddContentDistributionAspectCommand.builder()
						.url(getConfluencePageUrl(page)).modelComponentKey(concept.getKey())
						.type(ContentOutputType.CONFLUENCE_URL).title("Confluence page").build()));
		return page;
	}
}
