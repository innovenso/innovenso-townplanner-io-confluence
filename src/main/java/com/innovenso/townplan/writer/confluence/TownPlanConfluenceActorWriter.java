package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceActorWriter extends AbstractTownPlanConfluenceConceptWriter<BusinessActor> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceActorWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "actor"), confluencePageService, ConfluenceNavigationPageType.ACTOR);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(BusinessActor concept) {
		return "actor";
	}
}
