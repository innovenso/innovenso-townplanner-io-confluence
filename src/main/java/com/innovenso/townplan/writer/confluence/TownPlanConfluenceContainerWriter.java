package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceContainerWriter extends AbstractTownPlanConfluenceConceptWriter<ItContainer> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceContainerWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "container"), confluencePageService,
				ConfluenceNavigationPageType.CONTAINER);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(ItContainer concept) {
		return "container";
	}
}
