package com.innovenso.townplan.writer.confluence.model;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.aspects.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.domain.KeyPointInTimeImpl;
import com.innovenso.townplan.writer.confluence.util.ConfluenceUtils;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
public class ConceptConfluenceWriterModel<T extends Concept> extends ConceptWriterModel<T> {
	@Getter
	private final TechnologyRadarConfluenceWriterModel<T> radar;

	public ConceptConfluenceWriterModel(@NonNull T concept, @NonNull TownPlan townPlan) {
		super(concept, KeyPointInTimeImpl.builder().date(LocalDate.now()).build(), townPlan);
		this.radar = new TechnologyRadarConfluenceWriterModel<>(concept, townPlan);
	}

	public List<ContentDistribution> getDiagramImages() {
		return getConcept().getContentDistributions(ContentOutputType.SVG).stream()
				.sorted(Comparator.comparing(ContentDistribution::getSortKey)).collect(Collectors.toList());
	}

	public Set<ContentDistribution> getExcelFiles() {
		return getConcept().getContentDistributions(ContentOutputType.EXCEL);
	}

	public Set<ContentDistribution> getArchimateFiles() {
		return getConcept().getContentDistributions(ContentOutputType.ARCHIMATE);
	}

	public Set<ContentDistribution> getLatexFiles() {
		return getConcept().getContentDistributions(ContentOutputType.LATEX);
	}

	public Set<ContentDistribution> getZipFiles() {
		return getConcept().getContentDistributions(ContentOutputType.ZIP);
	}

	public boolean hasAttachments() {
		return !(getConcept().getAttachments().isEmpty() && getExcelFiles().isEmpty() && getArchimateFiles().isEmpty()
				&& getLatexFiles().isEmpty() && getZipFiles().isEmpty());
	}

	public String getAttachmentName(Attachment attachment) {
		return getFilename(attachment.getCdnContentUrl());
	}

	public String getConfluencePageTitle(Concept concept) {
		return ConfluenceUtils.getConfluencePageTitle(concept);
	}

	public String getFilename(@NonNull final ContentDistribution contentDistribution) {
		return getFilename(contentDistribution.getUrl());
	}

	private String getFilename(String url) {
		try {
			return url.substring(url.lastIndexOf('/') + 1);
		} catch (Exception e) {
			log.error("cannot get filename from url", e);
			return "";
		}
	}

	public List<SecurityImpact> getSecurityImpacts() {
		return getConcept().getSecurityImpacts().stream().sorted(Comparator.comparing(SecurityImpact::getSortKey))
				.collect(Collectors.toList());
	}

	public List<FunctionalRequirement> getFunctionalRequirements() {
		return getConcept().getFunctionalRequirements().stream()
				.sorted(Comparator.comparing(FunctionalRequirement::getSortKey)).collect(Collectors.toList());
	}

	public List<QualityAttributeRequirement> getQualityAttributeRequirements() {
		return getConcept().getQualityAttributes().stream()
				.sorted(Comparator.comparing(QualityAttributeRequirement::getSortKey)).collect(Collectors.toList());
	}

	public List<Constraint> getConstraints() {
		return getConcept().getConstraints().stream().sorted(Comparator.comparing(Constraint::getSortKey))
				.collect(Collectors.toList());
	}

	public List<Integer> getCostFiscalYears() {
		return getConcept().getCosts().stream().map(Cost::getFiscalYear).distinct().sorted()
				.collect(Collectors.toList());
	}

	public List<CostType> getCostTypes(@NonNull final Integer fiscalYear) {
		return getConcept().getCosts().stream().filter(cost -> fiscalYear.equals(cost.getFiscalYear()))
				.map(Cost::getCostType).distinct().sorted(Comparator.comparing(CostType::getLabel))
				.collect(Collectors.toList());
	}

	public List<Cost> getCosts(@NonNull final Integer fiscalYear, @NonNull final CostType costType) {
		return getConcept().getCosts().stream().filter(cost -> fiscalYear.equals(cost.getFiscalYear()))
				.filter(cost -> costType.equals(cost.getCostType())).sorted(Comparator.comparing(Cost::getCategory))
				.collect(Collectors.toList());
	}

	public Double getTotalCost(@NonNull final Integer fiscalYear, @NonNull final CostType costType) {
		return getCosts(fiscalYear, costType).stream().mapToDouble(Cost::getTotalCost).sum();
	}

	public Set<Cost> getCosts() {
		return getConcept().getCosts();
	}

	public List<SWOTType> getSwotTypes() {
		return SWOTType.getSwotTypes();
	}

	public Set<SWOT> getSwots(SWOTType swotType) {
		return getConcept().getSwots(Optional.ofNullable(swotType));
	}

	public Set<Decision> getRelatedDecisions() {
		return Optional.of(getConcept()).filter(Element.class::isInstance).map(Element.class::cast).stream()
				.flatMap(element -> element.getDirectDependencies(Decision.class).stream().map(Decision.class::cast))
				.collect(Collectors.toSet());
	}

	public List<Documentation> getDocumentations() {
		return getConcept().getDocumentations().stream()
				.sorted(Comparator.comparing(doc -> doc.getSortKey() == null ? doc.getKey() : doc.getSortKey()))
				.collect(Collectors.toList());
	}
}
