package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.writer.confluence.dto.Page;
import com.innovenso.townplan.writer.confluence.dto.request.CreateUpdateAttachmentRequest;
import com.innovenso.townplan.writer.confluence.dto.response.PagedAttachmentsResponse;
import com.innovenso.townplan.writer.confluence.dto.response.PagedPagesResponse;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import static feign.CollectionFormat.CSV;

public interface ConfluenceGateway {
	@RequestLine(value = "GET /space/{key}/content/page?depth=root&expand=version", collectionFormat = CSV)
	@Headers({"Content-Type: application/json; charset=UTF-8"})
	PagedPagesResponse getSpacePages(@Param("key") String key);

	@RequestLine("GET /content?spaceKey={key}&title={title}&type=page&expand=version,body.storage")
	@Headers({"Content-Type: application/json; charset=UTF-8"})
	PagedPagesResponse getPages(@Param("key") String key, @Param("title") String title);

	@RequestLine("POST /content")
	@Headers({"Content-Type: application/json; charset=UTF-8"})
	Page createPage(Page page);

	@RequestLine("DELETE /content/{id}")
	@Headers({"Content-Type: application/json; charset=UTF-8"})
	Page deletePage(@Param("id") String id);

	@RequestLine("DELETE /content/{id}?status=trashed")
	@Headers({"Content-Type: application/json; charset=UTF-8"})
	Page purgePage(@Param("id") String id);

	@RequestLine(value = "GET /content/{id}/child/page?expand=version,body.storage&limit=1000", collectionFormat = CSV)
	@Headers({"Content-Type: application/json; charset=UTF-8"})
	PagedPagesResponse getContentChildPages(@Param("id") String id);

	@RequestLine("PUT /content/{id}")
	@Headers({"Content-Type: application/json; charset=UTF-8"})
	Page updatePage(@Param("id") String id, Page page);

	@RequestLine("GET /content/{id}/child/attachment")
	PagedAttachmentsResponse getAttachmentsForPage(@Param("id") String id);

	@RequestLine("POST /content/{id}/child/attachment")
	@Headers({"X-Atlassian-Token: nocheck", "Content-Type: multipart/form-data"})
	void createAttachmentToPage(@Param("id") String id, CreateUpdateAttachmentRequest request);

	@RequestLine("PUT /content/{id}/child/attachment")
	@Headers({"X-Atlassian-Token: nocheck", "Content-Type: multipart/form-data"})
	void updateAttachmentToPage(@Param("id") String id, CreateUpdateAttachmentRequest request);

	@RequestLine("GET /content/{id}")
	@Headers({"Content-Type: application/json; charset=UTF-8"})
	Page getPage(@Param("id") String id);
}
