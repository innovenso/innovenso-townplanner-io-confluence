package com.innovenso.townplan.writer.confluence.dto;

public class PageBody {
	Storage storage;

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public enum Representation {
		STORAGE("storage");

		private final String key;

		Representation(String key) {
			this.key = key;
		}

		public String getKey() {
			return key;
		}

		@Override
		public String toString() {
			return getKey();
		}
	}

	public static class Storage {
		String value;
		Representation representation;

		public Representation getRepresentation() {
			return representation;
		}

		public void setRepresentation(Representation representation) {
			this.representation = representation;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
}
