package com.innovenso.townplan.writer.confluence;

import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.writer.confluence.dto.ConfluenceNavigationPageType;
import lombok.NonNull;

import java.io.File;

public class TownPlanConfluenceSystemIntegrationWriter
		extends
			AbstractTownPlanConfluenceConceptWriter<ItSystemIntegration> {
	private final TownPlanConfluenceNavigationService navigationService;

	public TownPlanConfluenceSystemIntegrationWriter(@NonNull File targetBaseDirectory,
			@NonNull final TownPlanConfluencePageService confluencePageService,
			@NonNull TownPlanConfluenceNavigationService navigationService) {
		super(new File(targetBaseDirectory, "integration"), confluencePageService,
				ConfluenceNavigationPageType.INTEGRATION);
		this.navigationService = navigationService;
	}

	@Override
	protected String getConfluenceWikiTemplateName(ItSystemIntegration concept) {
		return "integration";
	}
}
