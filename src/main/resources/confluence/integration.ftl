<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/pageLink.ftl" as ln>
<#import "lib/criticality.ftl" as crit>
<#import "lib/toc.ftl" as toc>
<#import  "lib/lifecycle.ftl" as life>
<#import "lib/verdict.ftl" as ver>
<#import "lib/conceptRelationships.ftl" as rel>
<#import "lib/security.ftl" as sec>
<#import "lib/documentation.ftl" as doc>
<#import "lib/attachments.ftl" as att>
<#import "lib/costs.ftl" as cost>
<#import "lib/swot.ftl" as swot>
<#import "lib/sparx.ftl" as sparx>
<#import "lib/fatherTime.ftl" as ft>

<@toc.toc concept=element.concept/>
<h2>Description</h2>
<p>Integration between <@ln.pageLink conceptModel=element targetConcept=element.concept.sourceSystem /> and <@ln.pageLink conceptModel=element targetConcept=element.concept.targetSystem /></p>
<@de.description concept=element />
<@doc.documentation conceptModel=element />
<@sparx.sparx sparxUrl=sparxUrl />

<h3>Criticality</h3>
<@crit.criticality integration=element.concept />
<@life.lifecycle concept=element.concept />
<@ver.verdict concept=element.concept />
<@sec.security conceptModel=element />
<@cost.costs conceptModel=element />
<@rel.conceptRelationships elementModel=element />
<@ch.children concept=element />
<@dg.diagrams concept=element />
<@swot.swots conceptModel=element />
<@att.attachments conceptModel=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />
