<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/attachments.ftl" as att>
<#import "lib/swot.ftl" as swot>
<#import "lib/fatherTime.ftl" as ft>

<@de.description concept=element />
<@ch.children concept=element />
<@dg.diagrams concept=element />
<@swot.swots conceptModel=element />
<@att.attachments conceptModel=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />
