<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/conceptRelationships.ftl" as rel>
<#import "lib/security.ftl" as sec>
<#import "lib/documentation.ftl" as doc>
<#import "lib/attachments.ftl" as att>
<#import "lib/costs.ftl" as cost>
<#import "lib/swot.ftl" as swot>
<#import "lib/decisionMetadata.ftl" as meta>
<#import "lib/decisionContexts.ftl" as con>
<#import "lib/decisionOptions.ftl" as opt>
<#import "lib/decisionConsequences.ftl" as cq>
<#import "lib/fatherTime.ftl" as ft>
<#import "lib/requirements.ftl" as req>


<ac:layout><ac:layout-section ac:type="single" ac:breakout-mode="default"><ac:layout-cell>
            <h2>Description</h2>
    <@de.description concept=element />
    <@doc.documentation conceptModel=element />

    <@meta.decisionMetadata conceptModel=element />
</ac:layout-cell>
</ac:layout-section>

    <@con.contexts conceptModel=element />

    <ac:layout-section ac:type="single"><ac:layout-cell>
        <@req.requirements conceptModel=element />
        </ac:layout-cell></ac:layout-section>

    <@opt.options conceptModel=element />

    <@cq.consequences conceptModel=element />

<ac:layout-section ac:type="single" ac:breakout-mode="default"><ac:layout-cell>
<@sec.security conceptModel=element />
<@dg.diagrams concept=element />
<@swot.swots conceptModel=element />
<@att.attachments conceptModel=element />
        <@ft.fatherTime conceptModel=element />
<@n.notice concept=element />
        </ac:layout-cell>
</ac:layout-section>
</ac:layout>