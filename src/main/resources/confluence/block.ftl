<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/toc.ftl" as toc>
<#import "lib/conceptRelationships.ftl" as rel>
<#import "lib/buildingblocks.ftl" as blk>
<#import "lib/capabilities.ftl" as caps>
<#import "lib/systems.ftl" as sys>
<#import "lib/attachments.ftl" as att>
<#import "lib/sparx.ftl" as sparx>
<#import "lib/fatherTime.ftl" as ft>

<@toc.toc concept=element.concept/>
<h2>Description</h2>
<@de.description concept=element />
<@sparx.sparx sparxUrl=sparxUrl />
<@caps.capabilities conceptModel=element capabilityList=element.concept.realizedCapabilities title="Capabilities realized by this Architecture Building Block" />
<@sys.systems conceptModel=element systemList=element.concept.realizingSystems title="IT Systems realizing this Architecture Building Block" />
<@ch.children concept=element />
<@dg.diagrams concept=element />
<@att.attachments conceptModel=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />
