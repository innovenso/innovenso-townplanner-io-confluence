<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/pageLink.ftl" as ln>
<#import "lib/toc.ftl" as toc>
<#import "lib/technologies.ftl" as tech>
<#import "lib/conceptRelationships.ftl" as rel>
<#import  "lib/lifecycle.ftl" as life>
<#import "lib/verdict.ftl" as ver>
<#import "lib/security.ftl" as sec>
<#import "lib/documentation.ftl" as doc>
<#import "lib/attachments.ftl" as att>
<#import "lib/swot.ftl" as swot>
<#import "lib/sparx.ftl" as sparx>
<#import "lib/fatherTime.ftl" as ft>
<#import "lib/techradar.ftl" as techradar>

<@toc.toc concept=element.concept/>
<h2>Description</h2>
<p>Part of <@ln.pageLink conceptModel=element targetConcept=element.concept.system /></p>
<@de.description concept=element />
<@doc.documentation conceptModel=element />
<@sparx.sparx sparxUrl=sparxUrl />
<@life.lifecycle concept=element.concept />
<@ver.verdict concept=element.concept />
<@tech.technologies technologyList=element.concept.technologies />
<@sec.security conceptModel=element />
<@rel.conceptRelationships elementModel=element />

<@ch.children concept=element />
<@dg.diagrams concept=element />
<@techradar.radar conceptModel=element />
<@swot.swots conceptModel=element />
<@att.attachments conceptModel=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />
