<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/capability.ftl" as cb>
<#import "lib/toc.ftl" as toc>
<#import "lib/conceptRelationships.ftl" as rel>
<#import "lib/attachments.ftl" as att>
<#import "lib/fatherTime.ftl" as ft>
<#import "lib/techradar.ftl" as techradar>


<@toc.toc concept=element.concept/>
<h2>Description</h2>
<@de.description concept=element />
<h2>Business Capabilities</h2>
<ul>
<#list element.concept.businessCapabilities as cap>
    <@cb.capability conceptModel=element businessCapability=cap />
</#list>
</ul>
<@ch.children concept=element />
<@dg.diagrams concept=element />
<@techradar.radar conceptModel=element />
<@att.attachments conceptModel=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />
