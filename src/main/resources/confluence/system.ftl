<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/toc.ftl" as toc>
<#import  "lib/lifecycle.ftl" as life>
<#import "lib/verdict.ftl" as ver>
<#import "lib/containers.ftl" as con>
<#import "lib/conceptRelationships.ftl" as rel>
<#import "lib/integrations.ftl" as ig>
<#import "lib/buildingblocks.ftl" as blk>
<#import "lib/security.ftl" as sec>
<#import "lib/documentation.ftl" as doc>
<#import "lib/attachments.ftl" as att>
<#import "lib/costs.ftl" as cost>
<#import "lib/swot.ftl" as swot>
<#import "lib/sparx.ftl" as sparx>
<#import "lib/fatherTime.ftl" as ft>
<#import "lib/techradar.ftl" as techradar>

<@toc.toc concept=element.concept/>
<h2>Description</h2>
<@de.description concept=element.concept />
<@doc.documentation conceptModel=element />
<@sparx.sparx sparxUrl=sparxUrl />
<@life.lifecycle concept=element.concept />
<@ver.verdict concept=element.concept />
<@sec.security conceptModel=element />
<@con.containers systemModel=element />
<@ig.integrations systemModel=element />
<@cost.costs conceptModel=element />
<@blk.blocks conceptModel=element buildingBlockList=element.concept.buildingBlocks title="Architecture Building Blocks realized by this System"/>
<@rel.conceptRelationships elementModel=element />
<@ch.children concept=element />
<@dg.diagrams concept=element />
<@techradar.radar conceptModel=element />
<@swot.swots conceptModel=element />
<@att.attachments conceptModel=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />

