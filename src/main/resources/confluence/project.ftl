<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/toc.ftl" as toc>
<#import "lib/conceptRelationships.ftl" as rel>
<#import "lib/milestones.ftl" as ms>
<#import "lib/security.ftl" as sec>
<#import "lib/documentation.ftl" as doc>
<#import "lib/attachments.ftl" as att>
<#import "lib/costs.ftl" as cost>
<#import "lib/swot.ftl" as swot>
<#import "lib/fatherTime.ftl" as ft>

<@toc.toc concept=element.concept/>
<h2>Description</h2>
<@de.description concept=element />
<@doc.documentation conceptModel=element />
<@ms.milestones conceptModel=element milestoneList=element.concept.milestones title="Milestones" />
<@sec.security conceptModel=element />
<@rel.conceptRelationships elementModel=element />
<@ch.children concept=element />
<@dg.diagrams concept=element />
<@swot.swots conceptModel=element />
<@att.attachments conceptModel=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />
