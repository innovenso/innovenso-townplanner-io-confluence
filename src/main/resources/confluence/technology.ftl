<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/toc.ftl" as toc>
<#import "lib/documentation.ftl" as doc>
<#import "lib/attachments.ftl" as att>
<#import "lib/swot.ftl" as swot>
<#import "lib/sparx.ftl" as sparx>
<#import "lib/fatherTime.ftl" as ft>
<#import "lib/recommendation.ftl" as reco>
<#import "lib/technologyType.ftl" as tt>

<@toc.toc concept=element.concept/>
<h2>Description</h2>
<p><@tt.type technology=element.concept /></p>
<p><@reco.recommendation tech=element.concept /></p>
<@de.description concept=element.concept />
<@doc.documentation conceptModel=element />
<@att.attachments conceptModel=element />
<@ch.children concept=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />

