<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/capability.ftl" as cb>
<#import "lib/toc.ftl" as toc>
<#import "lib/conceptRelationships.ftl" as rel>
<#import "lib/buildingblocks.ftl" as blk>
<#import "lib/attachments.ftl" as att>
<#import "lib/sparx.ftl" as sparx>
<#import "lib/fatherTime.ftl" as ft>


<@toc.toc concept=element.concept/>
<h2>Description</h2>
<p>Business Capability level ${element.concept.level}.</p>
<@de.description concept=element />
<@sparx.sparx sparxUrl=sparxUrl />
<h2>Child Capabilities</h2>
<ul>
    <#list element.concept.childCapabilities as cap>
        <@cb.capability conceptModel=element businessCapability=cap />
    </#list>
</ul>
<@blk.blocks conceptModel=element buildingBlockList=element.concept.buildingBlocksIncludingDescendants title="Architecture Building Blocks Realizing this capability or any of its children"/>
<@ch.children concept=element />
<@dg.diagrams concept=element />
<@att.attachments conceptModel=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />
