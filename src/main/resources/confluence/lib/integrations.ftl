<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>
<#import "criticality.ftl" as crit>

<#macro integrations systemModel>
    <#if systemModel.concept.integrations?has_content>
        <h2>Integrations with other systems</h2>
        <table data-layout="default"><colgroup><col style="width: 136.0px;" /><col style="width: 136.0px;" /><col style="width: 136.0px;" /><col style="width: 136.0px;" /><col style="width: 136.0px;" /></colgroup>
            <tbody>
            <tr>
                <th>
                    <p><strong>title</strong></p></th>
                <th>
                    <p><strong>other system</strong></p></th>
                <th>
                    <p><strong>implemented by (platform)</strong></p></th>
                <th>
                    <p><strong>description</strong></p></th>
                <th>
                    <p><strong>criticality</strong></p></th></tr>
        <#list systemModel.concept.integrations as integration>
            <tr>
                <td>
                    <p><@ln.pageLink conceptModel=systemModel targetConcept=integration />${integration.title}</p></td>
                <td>
                    <p><@ln.pageLink conceptModel=systemModel targetConcept=integration.getOther(systemModel.concept) /></p></td>
                <td>
                    <p></p></td>
                <td>
                    <p>${integration.description}</p></td>
                <td>
                    <p><@crit.criticality integration=integration /></p></td></tr>
        </#list>
            </tbody></table>

    </#if>

</#macro>
