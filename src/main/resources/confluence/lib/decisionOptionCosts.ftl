<#ftl output_format="XHTML">

<#macro costs conceptModel>
    <#if conceptModel.costs?has_content>
        <#list conceptModel.costFiscalYears as fiscalYear>
            <p><b>${fiscalYear}</b></p>
            <#list conceptModel.getCostTypes(fiscalYear) as costType>
                <p>${costType.label}</p>
                    <table><colgroup><col /><col /><col /><col /><col /><col /><col /></colgroup>
                        <tbody>
                        <tr>
                            <th>category</th>
                            <th>title</th>
                            <th colspan="1">description</th>
                            <th>unit of measure</th>
                            <th>cost per unit</th>
                            <th>number of units</th>
                            <th>total</th></tr>

                        <#list conceptModel.getCosts(fiscalYear, costType) as cost>
                            <tr>
                                <td>${cost.category}</td>
                                <td>${cost.title}</td>
                                <td colspan="1">${cost.description}</td>
                                <td>${cost.unitOfMeasure}</td>
                                <td>${cost.costPerUnit}</td>
                                <td>${cost.numberOfUnits}</td>
                                <td>${cost.totalCost}</td></tr>
                        </#list>

                        <tr>
                        <th style="text-align: right;" colspan="6">total ${costType.label} for ${fiscalYear}</th>
                        <th colspan="1">${conceptModel.getTotalCost(fiscalYear, costType)}</th></tr>
                        </tbody>
                    </table>
            </#list>
        </#list>
    </#if>
</#macro>