<#ftl output_format="XHTML">

<#macro pageLink conceptModel targetConcept>
    <ac:link>
        <ri:page ri:content-title="${conceptModel.getConfluencePageTitle(targetConcept)}" />
        <ac:plain-text-link-body>
            <![CDATA[${targetConcept.title}]]>
        </ac:plain-text-link-body>
    </ac:link>
</#macro>

