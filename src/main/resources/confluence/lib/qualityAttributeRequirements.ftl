<#ftl output_format="XHTML">

<#macro qualityAttributeRequirements conceptModel>
    <#if conceptModel.qualityAttributeRequirements?has_content>
        <h3>Quality Attribute Requirements</h3>
        <ac:structured-macro ac:name="tip" ac:schema-version="1"><ac:rich-text-body><p>Quality Attribute Requirements are described with architecture scenarios, as introduced in the book <a href="https://www.oreilly.com/library/view/continuous-architecture/9780128032855/">Continuous Architecture</a>. These scenarios how a system or component should <em>respond </em>when something happens (the <em>stimulus</em>), and within what <em>measure</em>. Optionally the <em>source</em> of the <em>stimulus</em> and the <em>environment</em> can be specified as well.</p></ac:rich-text-body></ac:structured-macro>
        <table><colgroup><col /><col /><col /><col/></colgroup>
            <tbody>
            <tr>
                <th>Quality Attribute</th>
                <th colspan="2">Architecture Scenario</th><th>Weight</th></tr>
            <#list conceptModel.qualityAttributeRequirements as req>
                <tr>
                    <th rowspan="5">${req.title}</th>
                    <td>Source of stimulus</td>
                    <td>${req.sourceOfStimulus}</td>
                <td rowspan="5">${req.weight.label}</td></tr>

                <tr>
                    <td>Stimulus</td>
                    <td>${req.stimulus}</td></tr>
                <tr>
                    <td>Environment</td>
                    <td>${req.environment}</td></tr>
                <tr>
                    <td>Response</td>
                    <td>${req.response}</td></tr>
                <tr>
                    <td>Response Measure</td>
                    <td>${req.responseMeasure}</td></tr>
            </#list>
            </tbody></table>
    </#if>
</#macro>





