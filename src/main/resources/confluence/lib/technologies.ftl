<#ftl output_format="XHTML">
<#import "recommendation.ftl" as rec>

<#macro technologies technologyList>
    <#if technologyList?has_content>
    <h2>Technologies</h2>
        <table data-layout="default"><colgroup><col style="width: 170.0px;" /><col style="width: 170.0px;" /><col style="width: 170.0px;" /><col style="width: 170.0px;" /></colgroup>
            <tbody>
            <tr>
                <th>
                    <p><strong>name</strong></p></th>
                <th>
                    <p><strong>description</strong></p></th>
                <th>
                    <p><strong>type</strong></p></th>
                <th>
                    <p><strong>architecture recommendation</strong></p></th></tr>
            <#list technologyList as tech>
            <tr>
                <td>
                    <p>${tech.title}</p></td>
                <td>
                    <p>${tech.description}</p></td>
                <td>
                    <p>${tech.technologyType.label}</p></td>
                <td>
                    <p><@rec.recommendation tech=tech /></p></td></tr>
            </#list>
</tbody></table>
    </#if>
</#macro>

