<#ftl output_format="XHTML">
<#import "relationships.ftl" as rel>

<#macro conceptRelationships elementModel>
        <#if elementModel.concept.relationships?has_content>
            <h2>Relationships</h2>
            <@rel.relationships conceptModel=elementModel concept=elementModel.concept relationshipList=elementModel.concept.incomingRelationships title="Incoming Relationships" otherTitle="from" />
            <@rel.relationships conceptModel=elementModel concept=elementModel.concept relationshipList=elementModel.concept.outgoingRelationships title="Outgoing Relationships" otherTitle="to" />
        </#if>
</#macro>

