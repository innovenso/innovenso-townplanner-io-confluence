<#ftl output_format="XHTML">

<#macro type technology>
        <ac:structured-macro ac:name="status" ac:schema-version="1" data-layout="default">
            <ac:parameter ac:name="title">${technology.technologyType.label}</ac:parameter>
            <ac:parameter ac:name="subtle">true</ac:parameter>
            <#switch technology.technologyType.value>
                <#case "TOOL">
                    <ac:parameter ac:name="colour">Green</ac:parameter>
                    <#break>
                <#case "TECHNIQUE">
                    <ac:parameter ac:name="colour">Blue</ac:parameter>
                    <#break>
                <#case "LANGUAGE_FRAMEWORK">
                <ac:parameter ac:name="colour">Purple</ac:parameter>
                    <#break>
                <#case "PLATFORM">
                <ac:parameter ac:name="colour">Red</ac:parameter>
                    <#break>
                <#default>
                <ac:parameter ac:name="colour">Gray</ac:parameter>
            </#switch>
        </ac:structured-macro>
</#macro>

