<#ftl output_format="XHTML">

<#macro recommendation tech>
    <#if tech.recommendation?has_content>
        <ac:structured-macro ac:name="status" ac:schema-version="1" data-layout="default">
            <ac:parameter ac:name="title">${tech.recommendation.label}</ac:parameter>
            <#switch tech.recommendation.value>
                <#case "ADOPT">
                    <ac:parameter ac:name="colour">Green</ac:parameter>
                    <#break>
                <#case "TRIAL">
                    <ac:parameter ac:name="colour">Blue</ac:parameter>
                    <#break>
                <#case "ASSESS">
                <ac:parameter ac:name="colour">Yellow</ac:parameter>
                    <#break>
                <#case "HOLD">
                <ac:parameter ac:name="colour">Red</ac:parameter>
                    <#break>
                <#default>
                <ac:parameter ac:name="colour">Gray</ac:parameter>
            </#switch>
        </ac:structured-macro>
    </#if>
</#macro>

