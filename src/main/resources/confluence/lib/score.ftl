<#ftl output_format="XHTML">

<#macro score percentage>
        <ac:structured-macro ac:name="status" ac:schema-version="1" data-layout="default">
            <ac:parameter ac:name="title">${percentage}%</ac:parameter>
            <#if percentage gt 80><ac:parameter ac:name="colour">Green</ac:parameter>
            <#elseif percentage gt 60><ac:parameter ac:name="colour">Yellow</ac:parameter>
            <#elseif percentage gt 40><ac:parameter ac:name="colour">Grey</ac:parameter>
            <#else><ac:parameter ac:name="colour">Red</ac:parameter>
            </#if>
        </ac:structured-macro>
</#macro>

