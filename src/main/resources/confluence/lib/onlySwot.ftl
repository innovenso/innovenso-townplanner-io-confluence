<#ftl output_format="XHTML">

<#macro swots concept>
    <#if concept.swots?has_content>
        <#list concept.swotTypes as swotType>
                <#list concept.getSwots(swotType) as swot>
                    <#switch swotType.value>
                        <#case "STRENGTH">
                            <ac:structured-macro ac:name="tip" ac:schema-version="1">
                                <ac:rich-text-body><p>${swot.description}</p></ac:rich-text-body></ac:structured-macro>
                            <#break>
                        <#case "WEAKNESS">
                        <ac:structured-macro ac:name="note" ac:schema-version="1">
                            <ac:rich-text-body><p>${swot.description}</p></ac:rich-text-body></ac:structured-macro>
                            <#break>
                        <#case "OPPORTUNITY">
                        <ac:structured-macro ac:name="info" ac:schema-version="1">
                            <ac:rich-text-body><p>${swot.description}</p></ac:rich-text-body></ac:structured-macro>
                            <#break>
                        <#case "THREAT">
                        <ac:structured-macro ac:name="warning" ac:schema-version="1">
                            <ac:rich-text-body><p>${swot.description}</p></ac:rich-text-body></ac:structured-macro>
                            <#break>
                        <#default>
                            <p>${swot.description}</p>
                    </#switch>
                </#list>
        </#list>
    </#if>
</#macro>