<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro radar conceptModel>
        <#if conceptModel.radar.hasTechnologies()>
            <h2>Technology Radar</h2>
        <#list conceptModel.radar.quadrants as quadrant>
                <h3>${quadrant.label}</h3>
                <#list conceptModel.radar.quadrantSections as section>
                    <h4>${section.label}</h4>
                    <ul>
                        <#list conceptModel.radar.getTechnologies(quadrant.value, section.value) as tech>
                            <li><p><@ln.pageLink conceptModel=conceptModel targetConcept=tech /></p></li>
                        </#list>
                    </ul>
                </#list>
        </#list>
        </#if>

</#macro>