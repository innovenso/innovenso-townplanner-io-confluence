<#ftl output_format="XHTML">

<#macro sparx sparxUrl>
        <#if sparxUrl?has_content>
    <ac:structured-macro ac:name="info" ac:schema-version="1" ><ac:parameter ac:name="title">Sparx Enterprise Architect</ac:parameter><ac:rich-text-body>
            <p><a href="${sparxUrl}">Open in Sparx WebEA</a></p></ac:rich-text-body></ac:structured-macro>
            </#if>
</#macro>