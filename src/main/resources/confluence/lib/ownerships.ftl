<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro ownerships teamModel>
<#if teamModel.concept.ownedSystems?has_content>
    <h2>Responsible for</h2>

<table data-layout="default"><colgroup><col style="width: 322.0px;" /><col style="width: 436.0px;" /></colgroup>
    <tbody>
    <tr>
        <th>
            <p><strong>system</strong></p></th>
        <th>
            <p><strong>description</strong></p></th></tr>
            <#list teamModel.concept.ownedSystems as system>

    <tr>
        <td>
            <p><@ln.pageLink conceptModel=teamModel targetConcept=system /></p></td>
        <td>
            <p>${system.description}</p></td></tr></#list>
    </tbody></table>
</#if>

</#macro>