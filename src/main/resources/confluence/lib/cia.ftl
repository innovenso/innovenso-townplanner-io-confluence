<#ftl output_format="XHTML">

<#macro cia conceptModel>
    <#if conceptModel.securityImpacts?has_content>
        <h3>CIA</h3>
        <table data-layout="default"><colgroup><col /><col /><col/></colgroup>
            <tbody>
        <#list conceptModel.securityImpacts as impact>
                <tr>
                <th>
                    <strong>${impact.impactType.label}</strong></th>
                <td>
                        <ac:structured-macro ac:name="status" ac:schema-version="1" data-layout="default">
            <ac:parameter ac:name="title">${impact.impactLevel.label}</ac:parameter>
            <#switch impact.impactLevel.value>
                <#case "EXTREME">
                    <ac:parameter ac:name="colour">Red</ac:parameter>
                    <#break>
                <#case "HIGH">
                    <ac:parameter ac:name="colour">Purple</ac:parameter>
                    <#break>
                <#case "MEDIUM">
                <ac:parameter ac:name="colour">Yellow</ac:parameter>
                    <#break>
                <#case "LOW">
                <ac:parameter ac:name="colour">Green</ac:parameter>
                    <#break>
                <#default>
                    <ac:parameter ac:name="colour">Gray</ac:parameter>
            </#switch>
        </ac:structured-macro>
                </td><td>${impact.description}</td></tr>

                </#list>
            </tbody></table>
    </#if>
</#macro>

