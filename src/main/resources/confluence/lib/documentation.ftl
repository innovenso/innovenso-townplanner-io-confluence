<#ftl output_format="XHTML">

<#macro documentation conceptModel>
        <#if conceptModel.documentations?has_content>
                <#list conceptModel.documentations as doc>
                            <#switch doc.type.value>
                                <#case "FUNCTIONAL">
                                    <p>${doc.description}</p>
                                    <#break>
                                <#case "DESCRIPTION">
                                    <p>${doc.description}</p>
                                    <#break>
                                <#case "API">
                                    <p><a href="${doc.description}">${doc.description}</a></p>
                                    <#break>
                                <#case "URL">
                                    <p><a href="${doc.description}">${doc.description}</a></p>
                                    <#break>
                            </#switch>
                </#list>
        </#if>
</#macro>




