<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro capability conceptModel businessCapability>
    <li><@ln.pageLink conceptModel=conceptModel targetConcept=businessCapability /> (level ${businessCapability.level})
        <#if businessCapability.childCapabilities?has_content>
            <ul>
                <#list businessCapability.childCapabilities as child>
                    <@capability conceptModel=conceptModel businessCapability=child />
                </#list>
            </ul>
        </#if>
    </li>
</#macro>
