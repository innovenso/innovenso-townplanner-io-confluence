<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>


<#macro relationships conceptModel concept relationshipList title otherTitle>
    <#if relationshipList?has_content>
        <h3>${title}</h3>
    </#if>
    <table data-layout="default"><colgroup><col style="width: 153.0px;" /><col style="width: 152.0px;" /><col style="width: 82.0px;" /><col style="width: 221.0px;" /><col style="width: 152.0px;" /></colgroup>
        <tbody>
        <tr>
            <th>
                <p><strong>${otherTitle}</strong></p></th>
            <th>
                <p><strong>title</strong></p></th>
            <th>
                <p><strong>type</strong></p></th>
            <th>
                <p><strong>description</strong></p></th>
            <th>
                <p><strong>technologies</strong></p></th></tr>

        <#list relationshipList as rel>
            <tr>
                <td>
                    <p><@ln.pageLink conceptModel=conceptModel targetConcept=rel.getOther(concept) /> </p></td>
                <td>
                    <p>${rel.title}</p></td>
                <td>
                    <p>${rel.relationshipType.label}</p></td>
                <td>
                    <p>${rel.description}</p></td>
                <td>
                    <p>${rel.technologyLabel}</p></td>
            </tr>
        </#list>
        </tbody>
    </table>
</#macro>

