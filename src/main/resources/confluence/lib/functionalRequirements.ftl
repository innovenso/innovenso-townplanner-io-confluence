<#ftl output_format="XHTML">

<#macro functionalRequirements conceptModel>
    <#if conceptModel.functionalRequirements?has_content>
        <h3>Functional Requirements</h3>
        <table><colgroup><col /><col /><col /></colgroup>
            <tbody>
            <#list conceptModel.functionalRequirements as req>
                <tr>
                    <th>${req.title}</th>
                    <td>
                        ${req.description}
                    </td>
                <td>${req.weight.label}</td></tr>
            </#list>
            </tbody></table>
    </#if>
</#macro>




