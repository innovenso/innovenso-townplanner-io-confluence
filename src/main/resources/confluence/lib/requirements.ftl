<#ftl output_format="XHTML">
<#import "cia.ftl" as cia>
<#import "functionalRequirements.ftl" as fun>
<#import "qualityAttributeRequirements.ftl" as qar>
<#import "constraints.ftl" as con>

<#macro requirements conceptModel>
    <h2>Requirements</h2>
    <@fun.functionalRequirements conceptModel=conceptModel />
    <@qar.qualityAttributeRequirements conceptModel=conceptModel />
    <@con.constraints conceptModel=conceptModel />
</#macro>

