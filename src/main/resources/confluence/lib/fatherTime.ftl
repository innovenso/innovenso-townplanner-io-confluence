<#ftl output_format="XHTML">

<#macro fatherTime conceptModel>
    <#if conceptModel.concept.fatherTimes?has_content>
        <h2>Father Time</h2>
                    <table><colgroup><col /><col /><col /><col /><col /></colgroup>
                        <tbody>
                        <tr>
                            <th>point in time</th>
                            <th>type</th>
                            <th>title</th>
                            <th>description</th>
                        </tr>
                        <#list conceptModel.concept.fatherTimes as ft>
                            <tr>
                                <td><time datetime="${ft.pointInTime.year?string.computer}-${ft.pointInTime.monthValue?string["00"]}-${ft.pointInTime.dayOfMonth?string["00"]}" /> </td>
                                <td>${ft.fatherTimeType.label}</td>
                                <td>${ft.title}</td>
                                <td>${ft.description}</td>
                            </tr>
                        </#list>

                        </tbody>
                    </table>
    </#if>
</#macro>