<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro consequences conceptModel>

<ac:layout-section ac:type="single">
    <ac:layout-cell>
        <h2>Consequences</h2>
        <ul>
            <#list conceptModel.consequences as context>
                <li><p>
                    <#if context.title?has_content><strong>${context.title}</strong>&#160;</#if>
                    <#if context.description?has_content>${context.description}</#if>
                    </p>
                </li>
            </#list>
        </ul>
    </ac:layout-cell>
</ac:layout-section>

</#macro>