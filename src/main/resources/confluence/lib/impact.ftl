<#ftl output_format="XHTML">
<#import "recommendation.ftl" as rec>
<#import "pageLink.ftl" as ln>

<#macro impact conceptModel relationshipList title>
    <#if relationshipList?has_content>
    <h3>${title}</h3>
        <table data-layout="default"><colgroup><col style="width: 170.0px;" /><col style="width: 170.0px;" /><col style="width: 170.0px;" /><col style="width: 170.0px;" /></colgroup>
            <tbody>
            <tr>
                <th>
                    <p><strong>element</strong></p></th>
                <th>
                    <p><strong>impact</strong></p></th>
                <th>
                    <p><strong>title</strong></p></th>
                <th>
                    <p><strong>description</strong></p></th></tr>
            <#list relationshipList as rel>
            <tr>
                <td>
                    <p><@ln.pageLink conceptModel=conceptModel targetConcept=rel.target /></p></td>
                <td>
                    <p>${rel.relationshipType.label}</p></td>
                <td>
                    <p>${rel.title}</p></td>
                <td>
                    <p>${rel.description}</p></td></tr>
            </#list>
</tbody></table>
    </#if>
</#macro>

