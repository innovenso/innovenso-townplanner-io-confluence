<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro capabilities conceptModel capabilityList title>
        <#if capabilityList?has_content>
        <h2>${title}</h2>

    <table data-layout="default"><colgroup><col style="width: 322px;" /><col style="width: 322px;" /></colgroup>
        <tbody>
        <tr>
            <th>
                <p><strong>name</strong></p></th>
            <th>
                <p><strong>description</strong></p></th></tr>

        <#list capabilityList as cap>
            <tr>
                <td>
                    <p><@ln.pageLink conceptModel=conceptModel targetConcept=cap /> (level ${cap.level})</p></td>
                <td>
                    <p>${cap.description}</p></td></tr>
        </#list>
        </tbody>
    </table>
</#if>
</#macro>
