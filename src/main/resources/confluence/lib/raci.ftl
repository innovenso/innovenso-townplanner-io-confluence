<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro raci conceptModel>
    <#if conceptModel.concept.raci?has_content>
        <h2>RACI</h2>
        <table data-layout="default"><colgroup><col style="width: 209.0px;" /><col style="width: 549.0px;" /></colgroup>
            <tbody>
            <tr>
                <th>
                    <p><strong>Responsible</strong></p></th>
                <td>
                    <ul>
                        <#list conceptModel.concept.responsibles as actor>
                            <li><@ln.pageLink conceptModel=conceptModel targetConcept=actor /></li>
                        </#list>
                    </ul>
                </td></tr>
            <tr>
                <th>
                    <p><strong>Accountable</strong></p></th>
                <td>
                    <ul>
                        <#list conceptModel.concept.accountables as actor>
                            <li><@ln.pageLink conceptModel=conceptModel targetConcept=actor /></li>
                        </#list>
                    </ul>

                </td></tr>
            <tr>
                <th>
                    <p><strong>Consulted</strong></p></th>
                <td>
                    <ul>
                        <#list conceptModel.concept.consulted as actor>
                            <li><@ln.pageLink conceptModel=conceptModel targetConcept=actor /> <ac:emoticon ac:name="tick" ac:emoji-shortname=":check_mark:" ac:emoji-id="atlassian-check_mark" ac:emoji-fallback=":check_mark:" /></li>
                        </#list>
                        <#list conceptModel.concept.toBeConsulted as actor>
                            <li><@ln.pageLink conceptModel=conceptModel targetConcept=actor /> <ac:emoticon ac:name="warning" ac:emoji-shortname=":warning:" ac:emoji-id="atlassian-warning" ac:emoji-fallback=":warning:" /></li>
                        </#list>
                    </ul>
                </td>
            </tr>
            <tr>
                <th>
                    <p><strong>Informed</strong></p></th>
                <td>
                    <ul>
                        <#list conceptModel.concept.informed as actor>
                            <li><@ln.pageLink conceptModel=conceptModel targetConcept=actor /> <ac:emoticon ac:name="tick" ac:emoji-shortname=":check_mark:" ac:emoji-id="atlassian-check_mark" ac:emoji-fallback=":check_mark:" /></li>
                        </#list>
                        <#list conceptModel.concept.toBeInformed as actor>
                            <li><@ln.pageLink conceptModel=conceptModel targetConcept=actor /> <ac:emoticon ac:name="warning" ac:emoji-shortname=":warning:" ac:emoji-id="atlassian-warning" ac:emoji-fallback=":warning:" /></li>
                        </#list>
                    </ul>

                </td></tr></tbody></table>
    </#if>
</#macro>

