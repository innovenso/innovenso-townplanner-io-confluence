<#ftl output_format="XHTML">

<#macro lifecycle concept>
    <#if concept.lifecycle?has_content>
        <h3>Lifecycle</h3>
        <ac:structured-macro ac:name="status" ac:schema-version="1" data-layout="default">
            <ac:parameter ac:name="title">${concept.lifecycle.type.label}</ac:parameter>
            <#switch concept.lifecycle.type.value>
                <#case "PLANNED">
                    <ac:parameter ac:name="colour">Blue</ac:parameter>
                    <#break>
                <#case "ACTIVE">
                    <ac:parameter ac:name="colour">Green</ac:parameter>
                    <#break>
                <#case "PHASEOUT">
                <ac:parameter ac:name="colour">Yellow</ac:parameter>
                    <#break>
                <#case "DECOMMISSIONED">
                <ac:parameter ac:name="colour">Red</ac:parameter>
                    <#break>
                <#default>
                <ac:parameter ac:name="colour">Gray</ac:parameter>
            </#switch>
        </ac:structured-macro>
        <p>${concept.lifecycle.description}</p>
        <#else>
        <p>${concept.lifecycle}</p>
    </#if>
</#macro>

