<#ftl output_format="XHTML">
<#import "decisionStatus.ftl" as stat>
<#import "pageLink.ftl" as ln>
<#import "toc.ftl" as toc>

<#macro decisionMetadata conceptModel>

<table data-layout="default"
    ><colgroup><col style="width: 208.0px;" /><col style="width: 550.0px;" /></colgroup>
    <tbody>
    <tr>
        <th>
            <p><strong>Status</strong></p></th>
        <td>
            <p><@stat.status decision=conceptModel.concept /></p></td></tr>

    <#if conceptModel.owners?has_content>
        <tr>
            <th>
                <p><strong>Owners</strong></p></th>
            <td>
                <ul>
                    <#list conceptModel.owners as person>
                        <li><@ln.pageLink conceptModel=conceptModel targetConcept=person /></li>
                    </#list>
                </ul>
            </td>
        </tr>
    </#if>

    <#if conceptModel.contributors?has_content>
        <tr>
            <th>
                <p><strong>Contributors</strong></p></th>
            <td>
                <ul>
                    <#list conceptModel.contributors as person>
                        <li><@ln.pageLink conceptModel=conceptModel targetConcept=person /></li>
                    </#list>
                </ul>
            </td>
        </tr>
    </#if>

    <#if conceptModel.approvers?has_content>
        <tr>
            <th>
                <p><strong>Approvers</strong></p></th>
            <td>
                <ul>
                    <#list conceptModel.approvers as person>
                        <li><@ln.pageLink conceptModel=conceptModel targetConcept=person /></li>
                    </#list>
                </ul>
            </td>
        </tr>
    </#if>

    <#if conceptModel.informed?has_content>
        <tr>
            <th>
                <p><strong>Informed</strong></p></th>
            <td>
                <ul>
                    <#list conceptModel.informed as person>
                        <li><@ln.pageLink conceptModel=conceptModel targetConcept=person /></li>
                    </#list>
                </ul>
            </td>
        </tr>
    </#if>


    <tr>
        <th>
            <p><strong>Outcome</strong></p></th>
        <td>
            <p>${conceptModel.concept.outcome}</p></td></tr>
    <tr>
        <th>
            <p><strong>On this page</strong></p></th>
        <td>
            <@toc.toc concept=conceptModel.concept/>
        </td></tr></tbody></table>


</#macro>
