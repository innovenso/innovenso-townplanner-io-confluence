<#ftl output_format="XHTML">
<#import "impact.ftl" as im>

<#macro impact conceptModel>
    <h2>Impact</h2>
        <@im.impact conceptModel=conceptModel relationshipList=conceptModel.capabilityImpacts title="Impacted Business Capabilities" />
        <@im.impact conceptModel=conceptModel relationshipList=conceptModel.buildingBlockImpacts title="Impacted Architecture Building Blocks" />
        <@im.impact conceptModel=conceptModel relationshipList=conceptModel.systemImpacts title="Impacted Systems" />
        <@im.impact conceptModel=conceptModel relationshipList=conceptModel.integrationImpacts title="Impacted System Integrations" />
</#macro>

