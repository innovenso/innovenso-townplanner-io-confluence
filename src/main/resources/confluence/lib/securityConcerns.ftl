<#ftl output_format="XHTML">

<#macro securityConcerns conceptModel>
    <#if conceptModel.concept.securityConcerns?has_content>
        <h3>Concerns</h3>
        <table data-layout="default"><colgroup><col /><col /><col/></colgroup>
            <tbody>
        <#list conceptModel.concept.securityConcerns as concern>
                <tr>
                <th>
                    <strong>${concern.concernType.label}</strong></th>
                <td>${concern.title}</td><td>${concern.description}</td></tr>

                </#list>
            </tbody></table>
    </#if>
</#macro>

