<#ftl output_format="XHTML">

<#macro constraints conceptModel>
    <#if conceptModel.constraints?has_content>
        <h3>Constraints</h3>
        <table><colgroup><col /><col /><col/></colgroup>
            <tbody>
            <#list conceptModel.constraints as req>
                <tr>
                    <th>${req.title}</th>
                    <td>
                        ${req.description}
                    </td><td>${req.weight.label}</td></tr>
            </#list>
            </tbody></table>
    </#if>
</#macro>




