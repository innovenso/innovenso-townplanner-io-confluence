<#ftl output_format="XHTML">

<#macro diagrams concept>
    <#if concept.diagramImages?has_content>
    <h2>Diagrams</h2>

    <#list concept.diagramImages as diagram>
        <h3>${diagram.title.orElse("")}</h3>
        <ac:image ac:align="center" ac:layout="full-width"><ri:attachment ri:filename="${concept.getFilename(diagram)}" /></ac:image>
    </#list>
    </#if>
</#macro>