<#ftl output_format="XHTML">

<#macro attachments concept conceptModel>
        <table><colgroup><col /><col /><col /></colgroup>
            <tbody>
            <#list concept.attachments as att>
                <tr>
                    <th>${att.title}</th>
                    <td>${att.description}</td>
                    <td><ac:link><ri:attachment ri:filename="${conceptModel.getAttachmentName(att)}" /></ac:link></td>
                </tr>
            </#list>
            </tbody></table>
</#macro>