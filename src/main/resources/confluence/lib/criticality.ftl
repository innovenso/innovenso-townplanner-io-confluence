<#ftl output_format="XHTML">

<#macro criticality integration>
    <#if integration.criticality.isPresent()>
        <ac:structured-macro ac:name="status" ac:schema-version="1" data-layout="default">
            <ac:parameter ac:name="title">${integration.criticality.get().label}</ac:parameter>
            <#switch integration.criticality.get().value>
                <#case "VERY_HIGH">
                    <ac:parameter ac:name="colour">Red</ac:parameter>
                    <#break>
                <#case "HIGH">
                    <ac:parameter ac:name="colour">Purple</ac:parameter>
                    <#break>
                <#case "MEDIUM">
                <ac:parameter ac:name="colour">Yellow</ac:parameter>
                    <#break>
                <#case "LOW">
                <ac:parameter ac:name="colour">Blue</ac:parameter>
                    <#break>
                <#case "VERY_LOW">
                    <ac:parameter ac:name="colour">Green</ac:parameter>
                    <#break>
                <#default>
                <ac:parameter ac:name="colour">Gray</ac:parameter>
            </#switch>
        </ac:structured-macro>
        <p>${integration.criticalityDescription.orElse("")}</p>
    </#if>
</#macro>

