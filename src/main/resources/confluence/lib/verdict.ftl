<#ftl output_format="XHTML">

<#macro verdict concept>
    <#if concept.architectureVerdict?has_content>
        <h3>Architecture Verdict</h3>
        <ac:structured-macro ac:name="status" ac:schema-version="1" data-layout="default">
            <ac:parameter ac:name="title">${concept.architectureVerdict.verdictType.label}</ac:parameter>
            <#switch concept.architectureVerdict.verdictType.value>
                <#case "TOLERATE">
                    <ac:parameter ac:name="colour">Blue</ac:parameter>
                    <#break>
                <#case "INVEST">
                    <ac:parameter ac:name="colour">Green</ac:parameter>
                    <#break>
                <#case "MIGRATE">
                <ac:parameter ac:name="colour">Yellow</ac:parameter>
                    <#break>
                <#case "ELIMINATE">
                <ac:parameter ac:name="colour">Red</ac:parameter>
                    <#break>
                <#default>
                <ac:parameter ac:name="colour">Gray</ac:parameter>
            </#switch>
        </ac:structured-macro>
        <p>${concept.architectureVerdict.description}</p>
    </#if>
</#macro>

