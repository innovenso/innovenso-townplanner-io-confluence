<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>


<#macro containers systemModel>
    <#if systemModel.concept.containers?has_content>
        <h2>Containers</h2>
        <table data-layout="default"><colgroup><col style="width: 170.0px;" /><col style="width: 170.0px;" /><col style="width: 170.0px;" /><col style="width: 170.0px;" /></colgroup>
        <tbody>
        <tr>
            <th>
                <p><strong>name</strong></p></th>
            <th>
                <p><strong>type</strong></p></th>
            <th>
                <p><strong>description</strong></p></th>
            <th>
                <p><strong>technologies</strong></p></th></tr>

        <#list systemModel.concept.containers as container>
            <tr>
                <td>
                    <p><@ln.pageLink conceptModel=systemModel targetConcept=container /> </p></td>
                <td>
                    <p>${container.type}</p></td>
                <td>
                    <p>${container.description}</p></td>
            <td><p>${container.technologyDescription}</p></td></tr>
        </#list>
        </tbody>
    </table>
    </#if>
</#macro>

