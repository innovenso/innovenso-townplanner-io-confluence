<#ftl output_format="XHTML">

<#macro status decision>
        <ac:structured-macro ac:name="status" ac:schema-version="1" data-layout="default">
            <ac:parameter ac:name="title">${decision.status.label}</ac:parameter>
            <#switch decision.status.value>
                <#case "DOING">
                    <ac:parameter ac:name="colour">Blue</ac:parameter>
                    <#break>
                <#case "DECIDED">
                    <ac:parameter ac:name="colour">Green</ac:parameter>
                    <#break>
                <#default>
                <ac:parameter ac:name="colour">Gray</ac:parameter>
            </#switch>
        </ac:structured-macro>
</#macro>

