<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>
<#import "onlySwot.ftl" as sw>
    <#import "decisionOptionCosts.ftl" as cost>
<#import "onlyAttachments.ftl" as oatt>
    <#import "score.ftl" as sc>

<#macro options conceptModel>

    <ac:layout-section ac:type="single" ac:breakout-mode="default"><ac:layout-cell>
<h2>Options</h2>
<table data-layout="default" ><colgroup><col style="width: 226.67px;" /><col style="width: 226.67px;" /><col style="width: 226.67px;" /></colgroup>
    <tbody>
    <tr>
        <th>
            <p /></th>
            <#list conceptModel.options as option>
                <th>
                    <p><strong>Option ${option?counter}</strong></p></th>
            </#list>
    </tr>

    <tr>
        <th>
            <p><strong>Summary</strong></p></th>
        <#list conceptModel.options as option>
            <td data-highlight-colour="${conceptModel.getColor(option)}">
                <p>${option.title}</p></td>
        </#list>
    </tr>
    <tr>
        <th>
            <p><strong>Description</strong></p></th>
        <#list conceptModel.options as option>
            <td data-highlight-colour="${conceptModel.getColor(option)}">
                <p>${option.description}</p></td>
        </#list>
</tr>
    <tr>
        <th>
            <p><strong>SWOT</strong></p></th>
        <#list conceptModel.options as option>
            <td data-highlight-colour="${conceptModel.getColor(option)}">
                <@sw.swots concept=option />
            </td>
        </#list>

    </tr>

    <tr>
        <th>
            <p><strong>Costs</strong></p></th>
        <#list conceptModel.options as option>
            <td data-highlight-colour="${conceptModel.getColor(option)}">
                <@cost.costs conceptModel=option />
            </td>
        </#list>
    </tr>

    <tr>
        <th>
            <p><strong>Attachments</strong></p></th>
        <#list conceptModel.options as option>
            <td data-highlight-colour="${conceptModel.getColor(option)}">
                <@oatt.attachments conceptModel=conceptModel concept=option />
            </td>
        </#list>
    </tr>

    <#list conceptModel.concept.functionalRequirements as req>
        <tr>
            <th><strong>${req.title} (${req.weight.label})</strong></th>
            <#list conceptModel.options as option>
                <td data-highlight-colour="${conceptModel.getColor(option)}">
                    <p>${option.getFunctionalRequirementScore(req.key).label}</p>
                </td>
            </#list>
        </tr>
    </#list>

    <#list conceptModel.concept.qualityAttributes as req>
        <tr>
            <th><strong>${req.title} (${req.weight.label})</strong></th>
            <#list conceptModel.options as option>
                <td data-highlight-colour="${conceptModel.getColor(option)}">
                    <p>${option.getQualityAttributeScore(req.key).label}</p>
                </td>
            </#list>
        </tr>
    </#list>

    <#list conceptModel.concept.constraints as req>
        <tr>
            <th><strong>${req.title} (${req.weight.label})</strong></th>
            <#list conceptModel.options as option>
                <td data-highlight-colour="${conceptModel.getColor(option)}">
                    <p>${option.getConstraintScore(req.key).label}</p>
                </td>
            </#list>
        </tr>
    </#list>

    <tr>
        <th>
            <p><strong>Requirement match</strong></p></th>
        <#list conceptModel.options as option>
            <td data-highlight-colour="${conceptModel.getColor(option)}">
                <p> <@sc.score percentage=option.totalScoreAsPercentage /></p></td>
        </#list>
    </tr>

    <tr>
        <th>
            <p><strong>Verdict</strong></p></th>
        <#list conceptModel.options as option>
            <td data-highlight-colour="${conceptModel.getColor(option)}">
                <p>${option.verdict.label}</p></td>
        </#list>

    </tr></tbody></table>
        </ac:layout-cell></ac:layout-section>
</#macro>