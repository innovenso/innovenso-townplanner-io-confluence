<#ftl output_format="XHTML">
<#import "onlySwot.ftl" as sw>

<#macro swots conceptModel>
    <#if conceptModel.concept.swots?has_content>
        <h2>SWOT</h2>
        <@sw.swots concept=conceptModel.concept />
    </#if>
</#macro>