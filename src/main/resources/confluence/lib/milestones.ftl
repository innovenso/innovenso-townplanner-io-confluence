<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro milestones conceptModel milestoneList title>
<#if milestoneList?has_content>
    <h2>${title}</h2>

<table data-layout="default"><colgroup><col style="width: 322.0px;" /><col style="width: 436.0px;" /></colgroup>
    <tbody>
    <tr>
        <th>
            <p><strong>name</strong></p></th>
        <th>
            <p><strong>description</strong></p></th></tr>
            <#list milestoneList as milestone>

    <tr>
        <td>
            <p><@ln.pageLink conceptModel=conceptModel targetConcept=milestone /></p></td>
        <td>
            <p>${milestone.description}</p></td></tr></#list>
    </tbody></table>
</#if>

</#macro>