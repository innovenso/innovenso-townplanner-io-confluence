<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>


<#macro members teamModel>
<#if teamModel.concept.getMembers()?has_content>
    <h2>Team Members</h2>


<table data-layout="default"><colgroup><col style="width: 226.67px;" /><col style="width: 226.67px;" /><col style="width: 226.67px;" /></colgroup>
    <tbody>
    <tr>
        <th>
            <p><strong>name</strong></p></th>
        <th>
            <p><strong>role</strong></p></th>
        <th>
            <p><strong>description</strong></p></th></tr>
        <#list teamModel.concept.getMembers() as member>
    <tr>
        <td>
            <p><@ln.pageLink conceptModel=teamModel targetConcept=member /> </p></td>
        <td>
            <p>${member.type}</p></td>
        <td>
            <p>${member.description}</p></td></tr>
            </#list>
</tbody></table>
</#if>

</#macro>