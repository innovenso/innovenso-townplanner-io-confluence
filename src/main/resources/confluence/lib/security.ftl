<#ftl output_format="XHTML">
<#import "cia.ftl" as cia>
<#import "securityConcerns.ftl" as con>

<#macro security conceptModel>
    <h2>Security</h2>
    <@cia.cia conceptModel=conceptModel />
    <@con.securityConcerns conceptModel=conceptModel />
</#macro>

