<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro contexts conceptModel>

<ac:layout-section ac:type="three_equal" ac:breakout-mode="default">
    <ac:layout-cell>
        <h2>Problem statement</h2>
        <ul>
            <#list conceptModel.currentStates as context>
                <li><p>
                    <#if context.title?has_content><strong>${context.title}</strong>&#160;</#if>
                    <#if context.description?has_content>${context.description}</#if>
                    </p>
                </li>
            </#list>
        </ul>
    </ac:layout-cell>
    <ac:layout-cell>
        <h2>Goals</h2>
        <ul>
            <#list conceptModel.goals as context>
                <li><p>
                        <#if context.title?has_content><strong>${context.title}</strong>&#160;</#if>
                        <#if context.description?has_content>${context.description}</#if>
                    </p>
                </li>
            </#list>
        </ul>
    </ac:layout-cell>
    <ac:layout-cell>
        <h2>Assumptions</h2>
        <ul>
            <#list conceptModel.assumptions as context>
                <li><p>
                        <#if context.title?has_content><strong>${context.title}</strong>&#160;</#if>
                        <#if context.description?has_content>${context.description}</#if>
                    </p>
                </li>
            </#list>
        </ul>
    </ac:layout-cell>
</ac:layout-section>

</#macro>