<#ftl output_format="XHTML">
<#import "decisionStatus.ftl" as stat>
<#import "pageLink.ftl" as ln>

<#macro decisions conceptModel>
    <#if conceptModel.relatedDecisions?has_content>
        <h2>Related Architecture Decisions</h2>
        <table><colgroup><col /><col /><col /><col /></colgroup>
            <tbody>
            <#list conceptModel.relatedDecisions as dec>
                <tr>
                    <th><@ln.pageLink conceptModel=conceptModel targetConcept=dec /></th>
                    <td>
                        ${dec.title}
                    </td>
                    <td>
                        <@stat.status decision=dec />
                    </td>
                    <td>
                        ${dec.outcome}
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>
    </#if>
</#macro>




