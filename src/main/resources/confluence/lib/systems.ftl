<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro systems conceptModel systemList title>
<#if systemList?has_content>
    <h2>${title}</h2>

<table data-layout="default"><colgroup><col style="width: 322.0px;" /><col style="width: 436.0px;" /></colgroup>
    <tbody>
    <tr>
        <th>
            <p><strong>system</strong></p></th>
        <th>
            <p><strong>description</strong></p></th></tr>
            <#list systemList as system>

    <tr>
        <td>
            <p><@ln.pageLink conceptModel=conceptModel targetConcept=system /></p></td>
        <td>
            <p>${system.description}</p></td></tr></#list>
    </tbody></table>
</#if>

</#macro>