<#ftl output_format="XHTML">

<#macro attachments conceptModel>
    <#if conceptModel.hasAttachments()>
        <h2>Attachments</h2>
        <table><colgroup><col /><col /><col /></colgroup>
            <tbody>
            <#list conceptModel.concept.attachments as att>
                <tr>
                    <th>${att.title}</th>
                    <td>${att.description}</td>
                    <td><ac:link><ri:attachment ri:filename="${conceptModel.getAttachmentName(att)}" /></ac:link></td>
                </tr>
            </#list>
            <#list conceptModel.archimateFiles as archi>
                <tr>
                    <th>Archimate export</th>
                    <td>An OpenExchange format export of the full town plan</td>
                    <td><ac:link><ri:attachment ri:filename="${conceptModel.getFilename(archi)}" /></ac:link></td>
                </tr>
            </#list>
            <#list conceptModel.excelFiles as excel>
                <tr>
                    <th>Excel export</th>
                    <td>A Microsoft Excel export of the full town plan</td>
                    <td><ac:link><ri:attachment ri:filename="${conceptModel.getFilename(excel)}" /></ac:link></td>
                </tr>
            </#list>
            <#list conceptModel.latexFiles as latex>
                <tr>
                    <th>LaTeX export</th>
                    <td>${latex.title.orElse("")}</td>
                    <td><ac:link><ri:attachment ri:filename="${conceptModel.getFilename(latex)}" /></ac:link></td>
                </tr>
            </#list>
            <#list conceptModel.zipFiles as zip>
                <tr>
                    <th>ZIP export</th>
                    <td>${zip.title.orElse("")}</td>
                    <td><ac:link><ri:attachment ri:filename="${conceptModel.getFilename(zip)}" /></ac:link></td>
                </tr>
            </#list>
            </tbody></table>
    </#if>

</#macro>