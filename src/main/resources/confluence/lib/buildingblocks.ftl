<#ftl output_format="XHTML">
<#import "pageLink.ftl" as ln>

<#macro blocks conceptModel buildingBlockList title>
        <#if buildingBlockList?has_content>
        <h2>${title}</h2>

    <table data-layout="default"><colgroup><col style="width: 322px;" /><col style="width: 322px;" /></colgroup>
        <tbody>
        <tr>
            <th>
                <p><strong>name</strong></p></th>
            <th>
                <p><strong>description</strong></p></th></tr>

        <#list buildingBlockList as block>
            <tr>
                <td>
                    <p><@ln.pageLink conceptModel=conceptModel targetConcept=block /> </p></td>
                <td>
                    <p>${block.description}</p></td></tr>
        </#list>
        </tbody>
    </table>
</#if>
</#macro>
