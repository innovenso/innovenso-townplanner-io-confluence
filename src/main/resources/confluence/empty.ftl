<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>

<@de.description concept=element />
<@ch.children concept=element />
<@dg.diagrams concept=element />
<@n.notice concept=element />
