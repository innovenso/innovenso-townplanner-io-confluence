<#ftl output_format="XHTML">
<#import "lib/notice.ftl" as n>
<#import "lib/diagrams.ftl" as dg>
<#import "lib/children.ftl" as ch>
<#import "lib/description.ftl" as de>
<#import "lib/toc.ftl" as toc>
<#import "lib/conceptRelationships.ftl" as rel>
<#import "lib/pageLink.ftl" as ln>
<#import "lib/raci.ftl" as raci>
<#import "lib/milestoneImpacts.ftl" as im>
<#import "lib/security.ftl" as sec>
<#import "lib/documentation.ftl" as doc>
<#import "lib/requirements.ftl" as req>
<#import "lib/attachments.ftl" as att>
<#import "lib/costs.ftl" as cost>
<#import "lib/swot.ftl" as swot>
<#import "lib/decisions.ftl" as dec>
<#import "lib/fatherTime.ftl" as ft>

<@toc.toc concept=element.concept/>
<h2>Description</h2>
<p>Milestone of the IT Project <@ln.pageLink conceptModel=element targetConcept=element.concept.project />.</p>
<@de.description concept=element />
<@doc.documentation conceptModel=element />
<@raci.raci conceptModel=element />
<@sec.security conceptModel=element />
<@req.requirements conceptModel=element />
<@dec.decisions conceptModel=element />
<@im.impact conceptModel=element />
<@cost.costs conceptModel=element />
<@ch.children concept=element />
<@dg.diagrams concept=element />
<@swot.swots conceptModel=element />
<@att.attachments conceptModel=element />
<@ft.fatherTime conceptModel=element />
<@n.notice concept=element />
